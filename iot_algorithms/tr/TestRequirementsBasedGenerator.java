package situations_generators.iot_algorithms.tr;

import com.mxgraph.model.mxICell;
import situations_generators.paper_algorithms.GraphAdapter;
import situations_generators.paper_algorithms.Path;
import situations_generators.paper_algorithms.generator_utils.TransformationUtils;
import situations_generators.paper_algorithms.setcovering_algorithms.AbstractSetCoveringGenerator;
import structure.GraphModelCore;
import structure.IoTCoverage;
import structure.Priority;
import structure.TestSituations;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Vaclav Rechtberger
 */
public class TestRequirementsBasedGenerator extends AbstractSetCoveringGenerator {
    //private GraphModelCore G;
    private float COP_treshold;
    private IoTCoverage criterion;

    public TestRequirementsBasedGenerator(GraphModelCore G, float COP_treshold, IoTCoverage criterion) {
        super(G,true);
        //this.G = G;
        this.COP_treshold = COP_treshold;
        this.criterion = criterion;
    }

    @Override
    protected Path convertNodePathToEdgePath(Path nodePath) {
        return TransformationUtils.convertNodePathToEdgesPathWithHighestPriority(nodePath, getModel(), Priority.LOW);
    }

    @Override
    public List<situations_generators.paper_algorithms.nanliimplementation.Path> getTestRequirements() {
        List<Path> R = RequirementUtils.getTestRequirements(getModel(),COP_treshold, criterion);
        return R.stream().map(path -> GraphAdapter.toNanliPath(path)).collect(Collectors.toList());
    }

    @Override
    protected TestSituations getTestSituation(List<Deque<mxICell>> ts) {
        List<Path> tr = RequirementUtils.getTestRequirements(getModel(), COP_treshold, criterion);
        HashMap<mxICell,List<Deque<mxICell>>> tr1 = new HashMap<>();

        //TODO is this optimalisation legit against spec. ? YES
        //Sort tr asc by length
        Collections.sort(tr, (o1, o2) -> o1.getSize() - o2.getSize());

        Path [] tr2 = tr.toArray(new Path[tr.size()]);
        for(int i = 0 ; i < tr2.length - 1 ; i++){
            Path iPath = tr2[i];
            boolean notRemoved = true;
            for(int j = i + 1 ; j < tr2.length && notRemoved ; j++){
                Path jPath = tr2[j];
                if(jPath.contains(iPath)){
                    tr.remove(iPath);
                    notRemoved = false;
                }
            }
        }

        for(Path path: tr){
            Path path1 = convertNodePathToEdgePath(path);
            mxICell first = path1.getFirstNode();
            List<Deque<mxICell>> tmp = tr1.get(first);
            if(tmp == null){
                tmp = new ArrayList<>();
                tr1.put(first,tmp);
            }
            tmp.add(new LinkedList<>(path1.getPathSteps()));
        }

        return new TestSituations(tr1,ts, Priority.LOW);
    }

}
