package situations_generators.iot_algorithms.tr;

import com.mxgraph.model.mxICell;
import situations_generators.iot_algorithms.LCZGraphUtils;
import situations_generators.iot_algorithms.LimitedConnectivityZone;
import situations_generators.paper_algorithms.Path;
import situations_generators.paper_algorithms.bruteforce_algorithms.GraphSearch;
import structure.GraphModelCore;
import structure.IoTCoverage;

import java.util.*;

/**
 * @author Vaclav Rechtberger, Matej Klima
 */
public class RequirementUtils {

    /**
     * Method creates a list of test requirements to satisfy the given coverage criteria.
     * @param G             SUT model
     * @param COP_treshold  connection outage probability treshold
     * @return list of test requirements
     */
    public static List<Path> getTestRequirements(GraphModelCore G, float COP_treshold) {
        List<Path> R = new ArrayList<>();
        LCZGraphUtils LCZGraphUtils = new LCZGraphUtils(G,COP_treshold);
        Set<mxICell> allOutNodes = LCZGraphUtils.getOutNodes();
        allOutNodes.add(G.getStart());
        Set<mxICell> allInNodes = LCZGraphUtils.getInNodes();
        allInNodes.addAll(LCZGraphUtils.getEndNodes());

        for (LimitedConnectivityZone L : LCZGraphUtils.getLCZs()) {
            List<Path> RL = new ArrayList<>();
            for(mxICell X :L.getInNodes()){
                for(mxICell Y: L.getOutNodes()){
                    Path path = findShortestPathInLCZ(G,X,Y,L); // Using BFS
                    if(path != null)
                        RL.add(path);
                }
            }
            R.addAll(RL);
        }
        return R;
    }

    /**
     * Method creates a list of test requirements to satisfy the given coverage criteria.
     * @param G             SUT model
     * @param COP_treshold  connection outage probability treshold
     * @return list of test requirements
     */
    public static List<Path> getTestRequirements(GraphModelCore G, float COP_treshold, IoTCoverage coverage) {
        if (coverage.equals(IoTCoverage.COMPACT_ALL_COMBINATIONS)) {
            return getTestRequirements(G, COP_treshold);
        } else {
            List<Path> R = new ArrayList<>();
            LCZGraphUtils LCZGraphUtils = new LCZGraphUtils(G, COP_treshold);
            Set<mxICell> allOutNodes = LCZGraphUtils.getOutNodes();
            allOutNodes.add(G.getStart());
            Set<mxICell> allInNodes = LCZGraphUtils.getInNodes();
            allInNodes.addAll(LCZGraphUtils.getEndNodes());

            for (LimitedConnectivityZone L : LCZGraphUtils.getLCZs()) {
                Set<Path> RLtemp = new TreeSet<>(Comparator.comparingInt(Path::getSize).thenComparingInt(Path::hashCode)); // keep the paths sorted
//                Set<Path> RLtemp = new TreeSet<>(Comparator.comparingInt(Path::getSize)); // TODO change to the previous command
                for (mxICell X : L.getInNodes()) {
                    for (mxICell Y : L.getOutNodes()) {
                        Path path = findShortestPathInLCZ(G, X, Y, L); // Using BFS
                        if (path != null)
                            RLtemp.add(path);
                    }
                }

                List<Path> RL = new ArrayList<>();
                Set<mxICell> insL = L.getInNodes();
                Set<mxICell> outsL = L.getOutNodes();
                for (Path p : RLtemp) {
                    mxICell in = p.getFirstNode();
                    mxICell out = p.getLastNode();
                    if (insL.remove(in)) {
                        outsL.remove(out);
                        RL.add(p);
                    } else if (outsL.remove(out)) {
                        RL.add(p);
                    }
                }
                R.addAll(RL);
            }
            return R;
        }
    }

    private static Path findShortestPathInLCZ(GraphModelCore G, mxICell in, mxICell out, LimitedConnectivityZone L) {
        FindShortestPathToEndViaLCZLogic logic = new FindShortestPathToEndViaLCZLogic(out,L,G);
        logic.execute(in, GraphSearch.getDefaultBFSContext());

        return logic.getResult();
    }

    private static class FindShortestPathToEndViaLCZLogic implements GraphSearch.VertexLogic<Path> {
        Path result;
        mxICell out;
        LimitedConnectivityZone LCZ;
        GraphModelCore G;
        Map<mxICell, Path> nodeToPathMap = new HashMap<>();

        public FindShortestPathToEndViaLCZLogic(mxICell out, LimitedConnectivityZone LCZ, GraphModelCore g) {
            this.out = out;
            this.LCZ = LCZ;
            G = g;
        }

        @Override
        public void execute(mxICell in, GraphSearch.Context context) {
            context.addNodeToExplore(in);
            Path path = new Path(in);
            nodeToPathMap.put(in,path);

            result = null;
            while(context.hasNextToExplore() && result == null){
                mxICell source = context.getNextNodeToExplore();
                Path actualPath = nodeToPathMap.get(source);
                Object [] outgoings = G.getGraph().getOutgoingEdges(source);
                mxICell [] LCZoutgoings = Arrays.stream(outgoings).filter(o -> LCZ.getEdges().contains(o)).toArray(mxICell[]::new);
                boolean DESTINATION = false;
                for(mxICell LCZoutgoing: LCZoutgoings){
                    mxICell destination = LCZoutgoing.getTerminal(DESTINATION);
                    if(!context.isDiscovered(destination)){
                        context.addToDiscovered(destination);
                        Path newPath = new Path(actualPath);
                        newPath.add(destination);
                        if(out.equals(destination)){
                            result = newPath;
                        }else{
                            nodeToPathMap.put(destination,newPath);
                            context.addNodeToExplore(destination);
                        }
                    }
                }
            }
        }

        @Override
        public Path getResult() {
            return result;
        }

        @Override
        public GraphSearch.Context getContext() {
            return GraphSearch.getDefaultBFSContext();
        }
    }
}
