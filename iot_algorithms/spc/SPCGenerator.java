package situations_generators.iot_algorithms.spc;

import com.mxgraph.model.mxICell;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import situations_generators.SituationsGeneratorInterface;
import situations_generators.iot_algorithms.LCZGraphUtils;
import situations_generators.iot_algorithms.LimitedConnectivityZone;
import situations_generators.iot_algorithms.TCsGenerationHelper;
import situations_generators.paper_algorithms.measuring.Stopwatches;
import structure.AbstractGraphCell;
import structure.GraphModelCore;
import structure.IoTCoverage;
import structure.TestSituations;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Class that drives the generation of test cases through LCPT algorithm in the graph of an IoT system.
 */
public class SPCGenerator implements SituationsGeneratorInterface {

    private static final Logger LOG = LogManager.getLogger();

    /**
     * List which holds all created test situations.
     */
    protected List<Deque<mxICell>> testSituations = new LinkedList<>();

    /**
     * LCZ in nodes and its shortest paths to the out nodes
     */
    protected Map<mxICell, Map<mxICell, Deque<mxICell>>> pathsInsideLczs = new HashMap<>();

    /**
     * Map of border nodes that were used in previous test situations to solve the EACH_IN_AND_OUT_ONCE
     * coverage criterion
     */
    private final Map<Integer, Set<mxICell>> outNodesUsedInTSs = new HashMap<>();
    private final Map<Integer, Set<mxICell>> inNodesUsedInTSs = new HashMap<>();

    private final Map<mxICell, Integer> lczNodesId = new HashMap<>();

    /**
     * Model is used for using its methods.
     */
    protected final GraphModelCore model;

    /**
     * Test coverage criterion
     */
    private final IoTCoverage coverage;
    private final AtomicBoolean isCanceled = new AtomicBoolean(false);

    /**
     * This variable stores the threshold for which  we  create  the  test cases T
     */
    private final float level;

    /**
     * Constructor for generating the test situations.
     *
     * @param model    Actual model instance in which the test situations should be found
     * @param coverage Coverage level of current test situations generations
     * @param level    the threshold for which  we  create  the  test cases T
     */
    public SPCGenerator(GraphModelCore model, IoTCoverage coverage, float level) {
        this.coverage = coverage;
        this.model = model;
        this.level = level;
    }


    /**
     * Finds all shortest paths between LCZ IN and OUT nodes
     * and saves them to the {@link SPCGenerator#pathsInsideLczs} variable.
     *
     * @param limitedConnectivityZones list of the found limited connectivity zones.
     */
    private void findAllShortestPathsInsideLCZs(List<LimitedConnectivityZone> limitedConnectivityZones) {
        for (LimitedConnectivityZone lcz : limitedConnectivityZones) {
            LOG.debug("Finding paths for LCZ id: " + lcz.getId());
            int noOfNodes = model.getNodes().size();
            Map<mxICell, Map<mxICell, Integer>> distancesToOuts = new HashMap<>(noOfNodes);
            // Map from OUT node to IN nodes with the distance (no of nodes between them)

            for (mxICell out : lcz.getOutNodes()) {
                ArrayDeque<mxICell> queue = new ArrayDeque<>();
                queue.add(out);
                distancesToOuts.put(out, new HashMap<>()); // distance from any node to OUT node
                Map<mxICell, Integer> distancesToOut = distancesToOuts.get(out);
                Map<mxICell, mxICell> pathsDescendats = new HashMap<>();
                distancesToOut.put(out, 0);
                while (!queue.isEmpty()) {
                    mxICell node = queue.pop();
                    Object[] parentEdges = model.getIncomingEdges(node);
                    for (Object edge : parentEdges) {
                        mxICell parentEdge = (mxICell) edge;
                        float parentEdgesProbability = ((AbstractGraphCell) parentEdge.getValue()).getLimitedConnectionProbability();
                        if (parentEdgesProbability >= level) {
                            mxICell parent = parentEdge.getTerminal(true);
                            if ((parent.equals(out) && (distancesToOut.get(parent) == 0 || distancesToOut.get(parent) > distancesToOut.get(node))) ||
                                    (!distancesToOut.containsKey(parent) || distancesToOut.get(parent) > distancesToOut.get(node))) {
//                            if (!distancesToOut.containsKey(parent) || distancesToOut.get(parent) > distancesToOut.get(node)) {
                                distancesToOut.put(parent, distancesToOut.get(node) + 1);
                                pathsDescendats.put(parent, node);
                                queue.add(parent);
                            }
                        }
                    }
                }
                distancesToOuts.put(out, distancesToOut);

                for (mxICell inNode : lcz.getInNodes()) {
                    if (distancesToOut.containsKey(inNode)) {
//                    if (distancesToOut.containsKey(inNode) && distancesToOut.get(inNode) > 0) {
                        Deque<mxICell> path = new ArrayDeque<>();
                        mxICell temp = inNode;
//                        if (temp == out) {
////                        if (temp == out && lcz.getInNodes().size() == 1 && lcz.getOutNodes().size() == 1) {
////                        if (distancesToOut.containsKey(inNode)) {
//                            // TODO refactor so that it works the same in all IoT algorithms
//                            // mind the case when LCZ has only one IN and OUT nodes and they are the same (84 - G9.1_|V|=40_D(G)=2)
//                            mxICell target = temp;
//                            mxICell bestTarget = null;
//                            int bestDistance = Integer.MAX_VALUE;
//                            for (mxICell outEdge : model.getOutgoingEdgesAsList(target)) {
//                                target = outEdge.getTerminal(false);
//                                if (distancesToOut.containsKey(target)) {
//                                    if (distancesToOut.get(target) <= bestDistance) {
//                                        bestDistance = distancesToOut.get(target);
//                                        bestTarget = target;
//                                    }
//                                }
//                            }
//                            pathsDescendats.put(temp, bestTarget);
//                            path.add(temp);
//                            temp = pathsDescendats.get(temp);
//                        }
                        if (pathsDescendats.containsKey(inNode)) { // there exists a valid LCZ path between inNode and out
                            if (out.equals(inNode)) { // skip to another node, so that the while below can work
                                path.add(temp);
                                temp = pathsDescendats.get(temp);
                            }
                            while (temp != out) {
                                path.add(temp);
                                temp = pathsDescendats.get(temp);
                            }
                            path.add(temp); // add end node
                            Map<mxICell, Deque<mxICell>> pathsToEnds = pathsInsideLczs.getOrDefault(inNode, new HashMap<>());
                            if (path.size() > 1) {
                                pathsToEnds.put(out, path);
                                pathsInsideLczs.put(inNode, pathsToEnds);
                            }

                        }
                    }
                }
            }
        }
    }

    /**
     * Test situations are constructed from paths between IN and OUT nodes in LCZs,
     * stored in the {@link SPCGenerator#pathsInsideLczs} variable.
     * <p>
     * Every test situation consists of edges from the graph's start node to some end node.
     *
     * @param limitedConnectivityZones A list of limited connectivity zones in the graph
     */
    private void createTestSituations(List<LimitedConnectivityZone> limitedConnectivityZones) {
        LOG.info("GENERATE TEST SITUATIONS");

        // Copy pathsInsideLCZ to another variable from which the paths will be gradually removed
        Map<mxICell, Map<mxICell, Deque<mxICell>>> pathsStack = new HashMap<>();
        for (mxICell lczIn : pathsInsideLczs.keySet()) {
            Map<mxICell, Deque<mxICell>> pathsMap = new HashMap<>();
            for (mxICell lczOut : pathsInsideLczs.get(lczIn).keySet()) {
                pathsMap.put(lczOut, pathsInsideLczs.get(lczIn).get(lczOut));
            }
            pathsStack.put(lczIn, pathsMap);
        }
        while (!pathsStack.isEmpty()) {
            LOG.info("--- SEARCHING FOR ANOTHER TEST SITUATION ---");
            LOG.debug("\tPATHS BEFORE: ");
            pathsStack.keySet().forEach(
                    key -> pathsStack.get(key).keySet().forEach(
                            mapKey -> LOG.debug("\t\t" + pathsStack.get(key).get(mapKey).stream().map(
                                    cell -> ((AbstractGraphCell) cell.getValue()).getName()).collect(Collectors.toList()))
                    )
            );
            Deque<mxICell> testSituation = findTestSituation(pathsStack, limitedConnectivityZones);
            if (testSituation != null) {
                LOG.info("\tTEST SITUATION FOUND: ");
                LOG.info(LCZGraphUtils.mxICellListToString(testSituation, ", "));
            } else {
                throw new IllegalStateException("The test situation wasn't successfuly found");
            }
            testSituations.add(testSituation);

        }
        LOG.info("TEST SITUATIONS GENERATED, PATH STACK EMPTY");
    }

    /**
     * Inner class used for representation of elements in a queue that is used during
     * {@link SPCGenerator#findTestSituation(Map, List)} method}
     */
    private static class QueueElement {
        final mxICell node;
        final mxICell edge;
        final QueueElement parent;

        QueueElement(mxICell node, mxICell edge, QueueElement parent) {
            this.node = node;
            this.edge = edge;
            this.parent = parent;
        }
    }

    /**
     * Construct a test situation out of remaining paths in LCZ zones in graph.
     * Traverse a graph and if an unused LCZ IN node is found, append its shortest path to the traversal.
     * Then continue until another LCZ IN node or graph's end node is found.
     * WARNING: the reach of unused LCZ IN node can't come through the LCZ edge.
     *
     * @param pathsStack               map with LCZ IN nodes as keys and MAP of LCZ OUT nodes and paths connecting them as values
     * @param limitedConnectivityZones A list with the limited connectivity zones in the graph
     */
    private Deque<mxICell> findTestSituation(Map<mxICell, Map<mxICell, Deque<mxICell>>> pathsStack, List<LimitedConnectivityZone> limitedConnectivityZones) {
        Deque<QueueElement> queue = new ArrayDeque<>();
        queue.add(new QueueElement(model.getStart(), null, null));

        boolean tcFound = false;

        while (!queue.isEmpty()) {
            if (isCanceled.get()) {
                return null;
            }
            QueueElement currElement = queue.poll();
            if (currElement != null) {
                if ((pathsStack.containsKey(currElement.node)) &&
                        (currElement.edge == null || ((AbstractGraphCell) currElement.edge.getValue()).getLimitedConnectionProbability() < level)) {
                    // traversed node is a LCZ IN node with unvisited LCZ path
                    tcFound = true;

                    // get the shortest path to some of the remaining OUT nodes from currently found IN node
                    Deque<mxICell> nextShortestPath = getNextShortestPathInLCZ(pathsStack, currElement.node, limitedConnectivityZones);
                    Iterator<mxICell> sptIterator = nextShortestPath.iterator();

                    sptIterator.next(); // skip the first node (temp is the same as currNode)
                    QueueElement nextQE = currElement;

                    while (sptIterator.hasNext()) { // iterate the path and save the connection between nodes
                        mxICell nextInSpt = sptIterator.next();
                        mxICell edge = model.getOutgoingEdgesAsList(nextQE.node).stream()
                                .filter(e -> e.getTerminal(false).equals(nextInSpt)).findFirst().orElse(null);
                        nextQE = new QueueElement(nextInSpt, edge, nextQE);
                    }

                    // clear the queue and continue from the child of the last element of the queue
                    queue.clear();
                    if (!model.getOutgoingEdgesAsList(nextQE.node).isEmpty()) {
                        for (mxICell childEdge : model.getOutgoingEdgesAsList(nextQE.node)) {
                            if (((AbstractGraphCell) childEdge.getValue()).getLimitedConnectionProbability() < level) {
                                QueueElement childQE = new QueueElement(childEdge.getTerminal(false), childEdge, nextQE);
                                queue.add(childQE);
                            }
                        }
                    } else {
                        LOG.debug("\tConstruct test situation from " + ((AbstractGraphCell) nextQE.node.getValue()).getName());
                        return constructTestSituation(nextQE);
                    }

                } else { // normal node, continue BFS traversing the graph
                    List<mxICell> outEdges = model.getOutgoingEdgesAsList(currElement.node);
                    if (outEdges.isEmpty()) { // end was reached
                        if (tcFound) {
                            LOG.debug("\tConstruct test situation from " + ((AbstractGraphCell) currElement.node.getValue()).getName());
                            return constructTestSituation(currElement);
                        }
                    } else {
                        for (mxICell outEdge : outEdges) {
                            mxICell target = outEdge.getTerminal(false);

                            QueueElement nextElement = new QueueElement(target, outEdge, currElement);
                            queue.add(nextElement);
                        }

                    }
                }
            }
        }

        throw new IllegalStateException("Test situation not constructable");
    }

    /**
     * Iterate from found end to graph's start and create new test situation out of it.
     *
     * @param endElement structure which contains links from the end to the graph's start node
     * @return path connecting start and end node.
     */
    private Deque<mxICell> constructTestSituation(QueueElement endElement) {
        Deque<mxICell> testSituation = new ArrayDeque<>();

        QueueElement temp = endElement;
        while (!temp.node.equals(model.getStart())) {
            testSituation.add(temp.node);
            temp = temp.parent;
        }
        testSituation.add(temp.node);    // add last node

        return testSituation;
    }

    /**
     * Finds next path from LCZ IN node in parameter to some of the remaining LCZ OUT nodes in a stack of paths.
     * Behaviour of this method is based on the criterion saved in {@link SPCGenerator#coverage}
     *
     * @param pathsStack               stack of paths that should be used in test situations
     * @param currNode                 LCZ IN node with unvisited LCZ path
     * @param limitedConnectivityZones a list with the limited connectivity zones in the graph
     * @return shortest path from LCZ IN node to some LCZ out node
     */
    private Deque<mxICell> getNextShortestPathInLCZ(Map<mxICell, Map<mxICell, Deque<mxICell>>> pathsStack, mxICell currNode, List<LimitedConnectivityZone> limitedConnectivityZones) {
        Map<mxICell, Deque<mxICell>> lczPathsFromIn = pathsStack.get(currNode); // key in this collection is OUT node
        int lczId = getNodesLczId(currNode, limitedConnectivityZones);
        // retrieve already used border nodes of this LCZ
        Set<mxICell> inNodesUsedInTSsInThisLCZ = inNodesUsedInTSs.getOrDefault(lczId, new HashSet<>());
        Set<mxICell> outNodesUsedInTSsInThisLCZ = outNodesUsedInTSs.getOrDefault(lczId, new HashSet<>());
        inNodesUsedInTSs.put(lczId, inNodesUsedInTSsInThisLCZ);
        outNodesUsedInTSs.put(lczId, outNodesUsedInTSsInThisLCZ);

        Iterator<mxICell> lczOutsIterator = lczPathsFromIn.keySet().iterator();
        mxICell lczOut = lczOutsIterator.next();
        if (coverage.isEachInAndOutNodeOnceCoverage()) {
            // find an unused OUT node for currently found IN node if there is any
            while (lczOutsIterator.hasNext() && outNodesUsedInTSsInThisLCZ.contains(lczOut)) {
                LOG.debug("For IN node: " + currNode.getValue()
                        + " skipping already used OUT node: " + lczOut.getValue());
                lczOut = lczOutsIterator.next();
                LOG.debug("New OUT node is: " + lczOut.getValue());
            }
        }
        Deque<mxICell> lczPath = lczPathsFromIn.get(lczOut); // selected next shortest path

        // remove a current path from lczPathsFromIn (and therefore pathsStack)
        LOG.info("Covering OUT node: " + lczOut.getValue() + " for IN node: " + currNode.getValue());
        lczPathsFromIn.remove(lczOut);
        // remove all paths that are contained in the found lcz path for the compact coverages
        if (coverage.isCompactCoverage()) {
            List<mxICell> lczPathList = new ArrayList<>(lczPath);
            for (int i = 0; i < lczPathList.size() - 1; i++) { // iterate the lczPath from the first to the pre last node
                mxICell node = lczPathList.get(i);
                if (pathsStack.containsKey(node)) {
                    Map<mxICell, Deque<mxICell>> lczPathsFromNode = pathsStack.get(node);
                    Iterator<mxICell> outsIterator = lczPathsFromNode.keySet().iterator();
                    while (outsIterator.hasNext()) {
                        mxICell out = outsIterator.next();
                        if (lczPathList.subList(i + 1, lczPathList.size()).contains(out)) { // path contains out after node
                            // found another IN OUT pair in the lczPath
                            LOG.info("Covering also OUT node: " + out.getValue()
                                    + " for IN node: " + node.getValue());
                            outsIterator.remove();
                            // add used border nodes to the list
                            inNodesUsedInTSsInThisLCZ.add(node);
                            outNodesUsedInTSsInThisLCZ.add(out);
                        }
                    }
                    // if there are no more paths beginning from current in node, remove it from the pathsStack
                    if (lczPathsFromNode.keySet().size() == 0) {
                        LOG.info("Removing IN node: " + node.getValue());
                        pathsStack.remove(node);
                    }
                }
            }
        }

        // add used border nodes to the list
        inNodesUsedInTSsInThisLCZ.add(currNode);
        outNodesUsedInTSsInThisLCZ.add(lczOut);

        // if there are no more paths beginning from current in node, remove it from the pathsStack
        if (lczPathsFromIn.keySet().size() == 0) {
            LOG.info("Removing IN node: " + currNode.getValue());
            pathsStack.remove(currNode);
        }
        if (coverage.isEachInAndOutNodeOnceCoverage()) {
            // remove all paths that go to covered OUT nodes or from this IN node and were already covered
            Iterator<mxICell> lczInIterator = pathsStack.keySet().iterator();
            while (lczInIterator.hasNext()) {
                mxICell currLczIn = lczInIterator.next();
                if (inNodesUsedInTSsInThisLCZ.contains(currLczIn)) { // LCZ IN node MUST be used at least ONCE
                    if (getNodesLczId(currLczIn, limitedConnectivityZones) == lczId) {
                        Iterator<mxICell> lczOutIterator = pathsStack.get(currLczIn).keySet().iterator();
                        while (lczOutIterator.hasNext()) {
                            //if (lczOutIterator.next().equals(lczOut)) {
                            mxICell currlczOut = lczOutIterator.next();
                            if (!currlczOut.equals(currLczIn)) {
                                if (outNodesUsedInTSsInThisLCZ.contains(currlczOut)) {
                                    LOG.info("Removing current OUT node: " + currlczOut.getValue()
                                            + " for IN node: " + currLczIn.getValue());
                                    lczOutIterator.remove();
                                }
                                if (pathsStack.get(currLczIn).size() == 0) {
                                    LOG.info("Removing IN node: " + currLczIn.getValue());
                                    lczInIterator.remove();
                                }
                            }
                        }
                    }
                }
            }
        }

        return lczPath;
    }

    @Override
    public TestSituations generateTestSituations() {
        try {
            LCZGraphUtils lczGraphUtils = new LCZGraphUtils(this.model, this.level);

            printLimitedConnectivityZones(lczGraphUtils.getLCZs());

            Stopwatches.getInstance().start();
            findAllShortestPathsInsideLCZs(lczGraphUtils.getLCZs());

            TCsGenerationHelper.printPathsInsideLczs(pathsInsideLczs);

            createTestSituations(lczGraphUtils.getLCZs());
            Stopwatches.getInstance().stop();

            return TCsGenerationHelper.convertTestSituationsFromNodes(lczGraphUtils.getLCZs(), testSituations, model);

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String stacktrace = sw.toString();
            LOG.error(stacktrace);
        }
        return null;
    }

    private int getNodesLczId(mxICell node, List<LimitedConnectivityZone> limitedConnectivityZones) {
        if (!lczNodesId.containsKey(node)) {
            for (LimitedConnectivityZone zone : limitedConnectivityZones) {
                if (zone.isIn(node) || zone.isOut(node) || zone.isInner(node)) {
                    lczNodesId.put(node, zone.getId());
                }
            }
        }
        return lczNodesId.get(node);
    }

    private void printLimitedConnectivityZones(List<LimitedConnectivityZone> limitedConnectivityZones) {
        for (LimitedConnectivityZone zone : limitedConnectivityZones) {
            LOG.info("Printing zone: " + zone);
        }
    }

    @Override
    public void setCancelled() {
        isCanceled.set(true);
    }
}
