package situations_generators.iot_algorithms.ant;

import com.mxgraph.model.mxICell;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import situations_generators.iot_algorithms.LCZGraphUtils;
import situations_generators.iot_algorithms.LimitedConnectivityZone;
import structure.AbstractGraphCell;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Math.pow;
import static situations_generators.iot_algorithms.ant.ANTGenerator.*;

/**
 * A class that represents a single ant (agent) in the ACO algorithm.
 */
public class Ant {

    private static final Logger LOG = LogManager.getLogger();

    private static final String[] names = {"Bella", "Coco", "Max", "Buddy", "Daisy", "Lola", "Angel", "Luna", "Lucy",
            "Harley", "Charlie", "Pepper", "Shadow", "Gracie", "Jack", "Milo", "Rocky", "Sadie", "Stella", "Nala",
            "Oliver", "Penny", "Ruby", "Chloe", "Cleo", "Duke", "Ginger", "Molly", "Sophie", "Baby", "Bear", "Bailey",
            "Maggie", "Marley", "Oscar", "Peanut", "Lucky", "Abby", "Cooper", "Dexter", "Loki", "Oreo", "Sammy",
            "Tucker", "Belle", "Leo", "Louie", "Romeo", "Sam", "Sasha", "Bandit", "Emma", "Finn", "Jake", "Jasper",
            "Kiwi", "Mia", "Scout", "Sunny", "Teddy", "Toby", "Willow", "Ariel", "Athena", "Bruce", "Dakota", "Diesel",
            "Gizmo", "Honey", "Jax", "Lady", "Minnie", "Murphy", "Piper", "Rosie", "Simba", "Spike", "Sunshine", "Zeus",
            "Ziggy", "Ace", "Apollo", "Buster", "Cookie", "Frankie", "George", "Henry", "Izzy", "Jackson", "Lulu",
            "Riley", "Thor", "Zoe", "Barney", "Bentley", "Bruno", "Dixie", "Hank", "Hazel", "Jade"};

    private final String name;
    private final mxICell initialPosition;
    private mxICell position;

    /**
     * Deque of edges that together makes the test case
     */
    private final Deque<mxICell> path;

    /**
     * Deque of edges inside the found LCZ
     */
    private final List<mxICell> pathInsideLcz;

    /**
     * Number of nodes in the path
     */
    private double cost;

    /**
     * the map with uncovered border nodes (IN nodes in the keys and set of reachable OUT nodes as values)
     */
    Map<mxICell, Set<mxICell>> remainingBorderNodesMap;

    /**
     * Map of LCZ IN nodes that are covered by this ant
     */
    private final Map<mxICell, Set<mxICell>> coveredOutNodes;

    /**
     * Map of LCZ OUT nodes that are covered by this ant
     */
    private final Set<mxICell> coveredInNodes;

    /**
     * The last IN node reached by the {@link #path}, or null
     */
    private mxICell lastReachedInNode;

    /**
     * Variable for generating pseudorandom numbers
     */
    private final Random rand;

    private final List<LimitedConnectivityZone> lczs;

    private final int id;

    /**
     * Init the variables and add the initial position to the path variable.
     *
     * @param initialPosition of the ant
     * @param id              of the ant
     * @param lczs            map with limited connectivity zones in the graph.
     */
    public Ant(mxICell initialPosition, int id, List<LimitedConnectivityZone> lczs, Map<mxICell, Set<mxICell>> remainingBorderNodesMap) {
        this.name = names[id];
        this.id = id;
        this.initialPosition = initialPosition;
        this.position = initialPosition;

        this.path = new LinkedList<>();
        this.pathInsideLcz = new LinkedList<>();
        this.cost = 0;
        this.coveredInNodes = new HashSet<>();
        this.coveredOutNodes = new HashMap<>();

        this.remainingBorderNodesMap = remainingBorderNodesMap;
        this.rand = new Random();
        this.lczs = lczs;
    }

    public Ant(Ant next) {
        this.name = next.name.concat("_champion");
        this.id = next.id;
        this.initialPosition = next.initialPosition;
        this.position = next.position;
        this.path = new ArrayDeque<>(next.path);
        this.pathInsideLcz = next.pathInsideLcz;
        this.cost = next.cost;
        this.coveredInNodes = new HashSet<>(next.coveredInNodes);
        this.coveredOutNodes = new HashMap<>(next.coveredOutNodes);
        this.remainingBorderNodesMap = new HashMap<>(next.remainingBorderNodesMap);
        this.rand = new Random();
        this.lczs = new ArrayList<>(next.lczs);
        this.lastReachedInNode = next.lastReachedInNode;
    }

    /**
     * Method that reset the state of an ant and puts it to the initial position
     */
    void reset() {
        this.position = this.initialPosition;
        this.path.clear();
        this.pathInsideLcz.clear();
        this.cost = 0;
        this.coveredInNodes.clear();
        this.coveredOutNodes.clear();
        this.lastReachedInNode = null;
    }

    /**
     * A crucial method that calculates the probabilities for the ant using the ACO algorithm formula and then moves
     * the ant to that found position.
     */
    void move() {
        // sum the desirability and pheromone levels of all of the possible transitions (denominator)
        LOG.trace("\tSelecting next node for node: " + this.position.getValue());

        double desirabilitySum = 0;
        double pheromoneSum = 0;
        double distanceSum = 0;

        Map<mxICell, Double> desirabilitiesMap = new HashMap<>();
        for (mxICell edge : model.getOutgoingEdgesAsList(this.position)) {
            desirabilitiesMap.put(edge, getDesirabilityOfTransition(edge));
        }

        for (mxICell edge : model.getOutgoingEdgesAsList(this.position)) {
            desirabilitySum += pow(desirabilitiesMap.get(edge), ALPHA);
            pheromoneSum += pow(pheromoneLevels.get(edge), BETA);
            distanceSum += pow(distanceLevels.get(edge), GAMA);
        }

        LOG.trace("\tSums: ∑τ=" + pheromoneSum + ", ∑η=" + desirabilitySum + ", ∑∆=" + distanceSum);
        // do the probabilities calculation
        Map<mxICell, Double> visitProbabilities = new HashMap<>();
        for (mxICell edge : model.getOutgoingEdgesAsList(this.position)) {
            double visitProbability; // the probability of a move using the currently traversed edge
            if (desirabilitySum > 0) { // there is some edge that reaches some unvisited LCZ node
                visitProbability = (pow(pheromoneLevels.get(edge), ALPHA) * pow(desirabilitiesMap.get(edge), BETA))
                        / (desirabilitySum * pheromoneSum);
            } else { // all outgoing edges reach no unvisited border nodes
                visitProbability = distanceLevels.get(edge);
            }

            LOG.trace("\t\tEdge: " + edge.getValue() + ", p=" + visitProbability
                    + "\t(τ=" + pheromoneLevels.get(edge)
                    + ", η=" + desirabilitiesMap.get(edge)
                    + ", ∆=" + distanceLevels.get(edge)
                    + ")");

            visitProbabilities.put(edge, visitProbability);
        }

        mxICell selectedTransition = chooseEdgeWithProbabilities(visitProbabilities);
        mxICell nextPosition = selectedTransition.getTerminal(false);
        LOG.trace("\tSelected node is: " + nextPosition.getValue() + " (edge: " + selectedTransition.getValue() + ")");
        // realize the move to the next position
        LOG.trace(this + "(" + this.getCost() + ", " + this.getCoveredBorderNodesCount() + ") is moving from: " + position.getValue() + " to " + nextPosition.getValue());
        position = nextPosition;
        if (((AbstractGraphCell) selectedTransition.getValue()).getLimitedConnectionProbability() >= level) {
            pathInsideLcz.add(selectedTransition);
        }
        path.add(selectedTransition);
        cost += 1;
    }

    /**
     * This method maintains the {@link #lastReachedInNode} variable during the creation of the current ant's path
     * and if it reaches the OUT node it calls the {@link #coverBorderNodesFromFoundOutNode(mxICell)} ()} method
     * to store the combination to the {@link #coveredInNodes} and {@link #coveredOutNodes} variables.
     * <p>
     * //     * @param remainingBorderNodesMap map with uncovered border nodes (IN node and points to a set of reachable OUT nodes).
     */
    void coverBorderNodes() {
        Iterator<mxICell> pathIterator = path.descendingIterator();
        final mxICell lastEdge = pathIterator.next();
        final mxICell source = lastEdge.getTerminal(true);
        final mxICell target = lastEdge.getTerminal(false);
        final AbstractGraphCell lastEdgeValue = (AbstractGraphCell) lastEdge.getValue();
        boolean lastEdgeLCZ = lastEdgeValue.getLimitedConnectionProbability() >= level;

        if (cost < 2) { // check the START node situation
            if (lastEdgeLCZ) { // START node is IN node
                if (remainingBorderNodesMap.containsKey(source)) { // START node is uncovered
                    lastReachedInNode = lastEdge.getTerminal(true);
                }
            }
        } else {
            final mxICell preLastEdge = pathIterator.next();
            final AbstractGraphCell preLastEdgeValue = (AbstractGraphCell) preLastEdge.getValue();
            if (preLastEdgeValue.getLimitedConnectionProbability() < level) { // check the IN node situation
                if (lastEdgeLCZ) {
                    if (remainingBorderNodesMap.containsKey(source)) { // IN node is uncovered
                        lastReachedInNode = source;
                    }
                }
            } else { // check the OUT node situation
                if (!lastEdgeLCZ) {
                    coverBorderNodesFromFoundOutNode(preLastEdge.getTerminal(false));
                    lastReachedInNode = null; // remove the reference to the in node that was already handled
                    pathInsideLcz.clear(); // clear the variable when leaving the zone
                }
            }
            // Handle the END node is OUT node situation
            if (model.getOutgoingEdgesAsList(target).isEmpty()) {
                if (lastEdgeLCZ) { // end node is OUT node
                    coverBorderNodesFromFoundOutNode(lastEdge.getTerminal(false));
                    lastReachedInNode = null; // remove the reference to the in node that was already handled
                    pathInsideLcz.clear(); // clear the variable when the END node was reached
                }
            }
        }
    }

    /**
     * Cover the IN and OUT border nodes pair.
     *
     * @param lczOut last LCZ OUT node found by the ant
     */
    private void coverBorderNodesFromFoundOutNode(mxICell lczOut) {
        if (lastReachedInNode != null) { // Uncovered IN node was found in previous rounds
            if (coverage.isComprehensiveCoverage()) {
                manageCoveredBorderNodesCollections(lastReachedInNode, lczOut);
            }
            if (coverage.isCompactCoverage()) {
                // cover the initial node
                for (int i = 0; i < pathInsideLcz.size(); i++) { // cover the rest
                    mxICell currEdge = pathInsideLcz.get(i);
                    mxICell inCandidate = currEdge.getTerminal(true);
                    ListIterator<mxICell> lczEdgeIterator = pathInsideLcz.listIterator(i);
                    mxICell outCandidate;
                    while (lczEdgeIterator.hasNext()) {
                        mxICell e = lczEdgeIterator.next();
                        outCandidate = e.getTerminal(false);
                        manageCoveredBorderNodesCollections(inCandidate, outCandidate);
                    }
                }
            }
        }
    }

    /**
     * Cover the in and out nodes in the parameters by adding them to the {@link #coveredInNodes} and {@link #coveredOutNodes}
     * collections, based on the current {@link ANTGenerator#coverage}.
     *
     * @param in  the in node to be covered
     * @param out the out node to be covered
     */
    private void manageCoveredBorderNodesCollections(mxICell in, mxICell out) {
        if (remainingBorderNodesMap.containsKey(in)) {
            // #1. handle OUT node; manage the coveredOutNodes collection
            if (remainingBorderNodesMap.get(in).contains(out)) { // new uncovered border nodes pair
                Set<mxICell> outNodesSet = coveredOutNodes.getOrDefault(in, new HashSet<>());
                coveredOutNodes.put(in, outNodesSet);
                outNodesSet.add(out);
                if (in.equals(out) && remainingBorderNodesMap.get(in).contains(in) && pathInsideLcz.size() > 0) { // cover the node itself
                    outNodesSet.add(in);
                }
            }
            if (coverage.isEachInAndOutNodeOnceCoverage()) {
                // iterate IN nodes and add it to covered border nodes if it points to the same OUT node
                for (mxICell uncovIn : remainingBorderNodesMap.keySet()) {
                    if (remainingBorderNodesMap.get(uncovIn).contains(out)) {
                        Set<mxICell> uncovInOutNodesSet = coveredOutNodes.getOrDefault(uncovIn, new HashSet<>());
                        coveredOutNodes.put(uncovIn, uncovInOutNodesSet);
                        uncovInOutNodesSet.add(out);
                    }
                }
            }

            // #2. handle IN node; manage the coveredInNode collection
            if (coverage.isEachInAndOutNodeOnceCoverage()) { // set the IN node as covered even though the OUT was covered already
                coveredInNodes.add(in);
            } else {
                if (coveredOutNodes.containsKey(in)) {
                    // set the IN node as covered only if all OUT nodes from the remainingBorderNodesMap were covered
                    if (coveredOutNodes.get(in).containsAll(remainingBorderNodesMap.get(in))) {
                        coveredInNodes.add(in);
                    }
                }
            }
        }
    }

//    /**
//     * Manage the coveredInNodes and coveredOutNodes collections for the COMPACT IoT coverages from the {@link IoTCoverage} class.
//     *
//     * @param coveredNode the IN node candidate
//     */
//    private void coverPathInsideLcz(mxICell coveredNode) {
//        // coveredNode as an OUT node candidate first
//        if (coverage.equals(IoTCoverage.COMPACT_EACH_IN_AND_OUT_ONCE)) {
//            // iterate uncovered IN nodes
//            for (mxICell uncovIn : remainingBorderNodesMap.keySet()) {
//                if (remainingBorderNodesMap.get(uncovIn).contains(coveredNode)) {
//                    // add it to covered border nodes if it points to the node that is being iterated
//                    Set<mxICell> uncovOutNodesSet = coveredOutNodes.getOrDefault(uncovIn, new HashSet<>());
//                    uncovOutNodesSet.add(coveredNode);
//                    coveredOutNodes.put(uncovIn, uncovOutNodesSet);
//                }
//            }
//        }
//        if (remainingBorderNodesMap.containsKey(coveredNode)) { // coveredNode is uncovered IN node
//            for (mxICell potentialOut : remainingBorderNodesMap.get(coveredNode)) { // does a pathInsideLcz contain any OUT?
//                if (pathInsideLcz.contains(potentialOut)) {
//                    Set<mxICell> uncovInOutNodesSet = coveredOutNodes.getOrDefault(coveredNode, new HashSet<>());
//                    uncovInOutNodesSet.add(potentialOut);
//                    coveredOutNodes.put(coveredNode, uncovInOutNodesSet);
//                }
//            }
//            // cover the IN nodes
//            if (coverage.equals(IoTCoverage.COMPACT_EACH_IN_AND_OUT_ONCE)) {
//                coveredInNodes.add(coveredNode);
//            } else {
//                if (coveredOutNodes.containsKey(coveredNode)) {
//                    if (coveredOutNodes.get(coveredNode).size() == remainingBorderNodesMap.get(coveredNode).size()) {
//                        coveredInNodes.add(coveredNode);
//                    }
//                }
//            }
//        }
//    }

    /**
     * Chooses a next edge for the ant a following way:
     * 1) normalizes the probabilities
     * 2) generates random number
     * 3) sums the probabilities of the edges until the sum is not bigger or equal to the generated random number
     *
     * @param visitProbabilities a map with edges and its probabilities of selection
     * @return selected edge with the probabilities taken into account.
     */
    private mxICell chooseEdgeWithProbabilities(Map<mxICell, Double> visitProbabilities) {
        // at first change the probabilities to sum up to 1.0
        double sum = visitProbabilities.values().stream().mapToDouble(d -> d).sum();
        visitProbabilities.replaceAll((k, v) -> visitProbabilities.get(k) / sum);
        double randNum = rand.nextDouble();
        double tempSum = 0;
        Iterator<mxICell> edgeIterator = visitProbabilities.keySet().iterator();
        mxICell edge = null;
        while (tempSum < randNum) {
            edge = edgeIterator.next();
            tempSum += visitProbabilities.get(edge);
        }
        if (edge != null) {
            return edge;
        } else {
            throw new IllegalStateException("Unable to select next edge for current ant");
        }
    }

    /**
     * Finds whether the position of the ant is in any of the graph's end nodes.
     *
     * @return true when there are no outgoing edges from the current position.
     */
    boolean endReached() {
        for (int i = 0; i < position.getEdgeCount(); i++) {
            if (position.getEdgeAt(i).getTerminal(true).equals(position)) {
                return false;
            }
        }
        return true;
    }

    public void printPath(String comment, Level logLevel) {
        final Set<mxICell> coveredOutNodesSet = new HashSet<>();
        coveredOutNodes.values().forEach(coveredOutNodesSet::addAll);
        LOG.log(logLevel, comment + this.name + "'s path(cost=" + cost + ", gain=" + getCoveredBorderNodesCount()
                + ", in=" + coveredInNodes.stream().map(n -> n.getValue().toString()).collect(Collectors.toList())
                + ", out=" + coveredOutNodesSet.stream().map(n -> n.getValue().toString()).collect(Collectors.toList())
                + ") is: " + LCZGraphUtils.mxICellListToString(path, " - "));
    }

    /**
     * Calculates the desirability of the edge according to the number of border nodes reachable from surrounding nodes
     * and not covered yet.
     *
     * @param edge whose desirability should be calculated
     * @return desirability of the edge
     */
    public double getDesirabilityOfTransition(mxICell edge) {
        mxICell source = edge.getTerminal(true);
        mxICell target = edge.getTerminal(false);
        boolean isLCZEdge = ((AbstractGraphCell) edge.getValue()).getLimitedConnectionProbability() >= level;
        boolean isPrevLCZEdge = cost > 0 && ((AbstractGraphCell) path.getLast().getValue()).getLimitedConnectionProbability() >= level;
        LimitedConnectivityZone currentLCZ = lczs.stream().filter(limitedConnectivityZone -> limitedConnectivityZone.getEdges().contains(edge)).findFirst().orElse(null);
        boolean isSourceUncoveredInNode = remainingBorderNodesMap.containsKey(source) && !coveredInNodes.contains(source);
        boolean isSourceUncoveredOutNode = remainingBorderNodesMap.values().stream().anyMatch(set -> set.contains(source));

        // 1st part of the algorithm; init the uncovered IN and OUT nodes reachable from target node variables
        // Find uncovered border nodes reachable from the target node
        Set<mxICell> uncoveredInNodesReachableFromTarget = new HashSet<>();
        if (reachableInNodesMap.containsKey(target) && !reachableInNodesMap.get(target).isEmpty()) {
            uncoveredInNodesReachableFromTarget.addAll(reachableInNodesMap.get(target).stream().filter(n -> !coveredInNodes.contains(n)).collect(Collectors.toSet()));
        }
        Set<mxICell> uncoveredOutNodesReachableFromTarget = new HashSet<>();
        if (reachableOutNodesMap.containsKey(target)) {
            reachableOutNodesMap.get(target).forEach(n -> {
                // do not count the already covered OUT nodes
                if (coveredOutNodes.values().stream().noneMatch(set -> set.contains(n))) {
                    uncoveredOutNodesReachableFromTarget.add(n);
                }
            });
        }

        // 2nd part of the algorithm; edit the variables based on the current situation (IN zone / OUT / On the border)
        // Also, print the state of the variables and sum everything to the noOfUncoveredBorderNodesReachable
        double noOfUncoveredBorderNodesReachable = 0; // total number of reachable uncovered border nodes when using this edge
        if (isLCZEdge) { // IN, or on the border
            if (currentLCZ == null) {
                throw new IllegalStateException("The ant " + this + " reached LCZ edge: " + edge + " but current LCZ is null");
            }
            if (lastReachedInNode != null) { // IN
                // the ant is currently in the zone and entered it by the uncovered IN node
                // if source is OUT node, remove it, so the ant will not continue through the zone
                uncoveredOutNodesReachableFromTarget.remove(source);
                // if currently inside the zone, do not count IN nodes that lead to this zone
                uncoveredInNodesReachableFromTarget.removeAll(currentLCZ.getInNodes());
                // if the target node is uncovered out node, append one
                noOfUncoveredBorderNodesReachable += 1;
                // add the number of uncovered IN and OUT nodes reachable to the variable
                noOfUncoveredBorderNodesReachable += uncoveredInNodesReachableFromTarget.size() + uncoveredOutNodesReachableFromTarget.size();
                LOG.trace("\t\tEdge: " + edge.getValue() + "; INSIDE LCZ, IN nodes: "
                        + uncoveredInNodesReachableFromTarget.stream().map(bn -> bn.getValue().toString()).collect(Collectors.toList())
                        + "; uncovered OUT nodes: "
                        + uncoveredOutNodesReachableFromTarget.stream().map(bn -> bn.getValue().toString()).collect(Collectors.toList()));
            } else if (!isPrevLCZEdge && isSourceUncoveredInNode) { // OUT -> IN
                // if the LCZ edge points to the uncovered IN node (this way it cannot be covered)
                uncoveredInNodesReachableFromTarget.remove(target);
                // append one, because the source is uncovered IN node
                noOfUncoveredBorderNodesReachable += 1;
                // add the number of uncovered IN and OUT nodes reachable to the variable
                noOfUncoveredBorderNodesReachable += uncoveredInNodesReachableFromTarget.size() + uncoveredOutNodesReachableFromTarget.size();
                LOG.trace("\t\tEdge: " + edge.getValue() + "; OUTSIDE -> INSIDE, IN nodes: "
                        + uncoveredInNodesReachableFromTarget.stream().map(bn -> bn.getValue().toString()).collect(Collectors.toList())
                        + "; uncovered OUT nodes: "
                        + uncoveredOutNodesReachableFromTarget.stream().map(bn -> bn.getValue().toString()).collect(Collectors.toList()));
            } else { // in the zone, but not entered by the uncovered IN node; remove all OUT nodes of current zone
                uncoveredInNodesReachableFromTarget.removeAll(currentLCZ.getOutNodes());
                LOG.trace("\t\tEdge: " + edge.getValue() + "; INSIDE LCZ but without entering by uncovered IN node, IN nodes: "
                        + uncoveredInNodesReachableFromTarget.stream().map(bn -> bn.getValue().toString()).collect(Collectors.toList())
                        + "; uncovered OUT nodes: "
                        + uncoveredOutNodesReachableFromTarget.stream().map(bn -> bn.getValue().toString()).collect(Collectors.toList()));
                noOfUncoveredBorderNodesReachable += uncoveredInNodesReachableFromTarget.size();
            }
        } else if (lastReachedInNode != null &&
                (isSourceUncoveredOutNode || coverage.isEachInAndOutNodeOnceCoverage())) { // IN -> OUT
            // TODO check if the E I O O coverage is enough
            // if the edge is not LCZ ant can leave the zone and cover some OUT node
            noOfUncoveredBorderNodesReachable += 1;
            LOG.trace("\t\tEdge: " + edge.getValue() + "; INSIDE -> OUTSIDE, IN nodes: "
                    + uncoveredInNodesReachableFromTarget.stream().map(bn -> bn.getValue().toString()).collect(Collectors.toList())
                    + "; uncovered OUT nodes: "
                    + uncoveredOutNodesReachableFromTarget.stream().map(bn -> bn.getValue().toString()).collect(Collectors.toList()));
            noOfUncoveredBorderNodesReachable += uncoveredInNodesReachableFromTarget.size() + uncoveredOutNodesReachableFromTarget.size();
        } else if (lastReachedInNode == null && isSourceUncoveredInNode) { // OUT -> IN
            // on the border of zone, enter it by another edge and don't pick this non-LCZ edge
            LOG.trace("\t\tEdge: " + edge.getValue() + "; OUTSIDE -> OUSTIDE, do not use it");
            return 0;
        } else { // OUT
            LOG.trace("\t\tEdge: " + edge.getValue() + "; OUTSIDE LCZ, uncovered IN nodes: "
                    + uncoveredInNodesReachableFromTarget.stream().map(bn -> bn.getValue().toString()).collect(Collectors.toList()));
//                    + "; uncovered OUT nodes: "
//                    + uncoveredOutNodesReachableFromTarget.stream().map(bn -> bn.getValue().toString()).collect(Collectors.toList()));
            noOfUncoveredBorderNodesReachable += uncoveredInNodesReachableFromTarget.size();

        }

        if (noOfUncoveredBorderNodesReachable > 0) { // there are some border nodes reachable from the target
            return (1.0 / (-2 * noOfUncoveredBorderNodesReachable)) + 1;
//            return (-1.0 / (noOfUncoveredBorderNodesReachable + distanceLevels.get(edge))) + 1; // include the distance to end node in the calculation
        }

        return 0;
    }

    /**
     * Prints the desirabilities of all edges in a graph by calling the {@link #getDesirabilityOfTransition(mxICell)}
     * when the Ant moves at least once.
     */
    @SuppressWarnings("unused")
    void printCurrentDesirabilities() {
        StringBuilder sb = new StringBuilder();
        for (mxICell edge : model.getEdges()) {
            sb.append(edge.getValue()).append(": ")
                    .append(getDesirabilityOfTransition(edge))
                    .append(", ");
        }
        LOG.debug("Desirability levels: ");
        LOG.debug(sb.toString());
    }

    @Override
    public String toString() {
        return name + "#" + id;
    }

    public Deque<mxICell> getPath() {
        return path;
    }

    public double getCost() {
        return cost;
    }

    public Map<mxICell, Set<mxICell>> getCoveredOutNodes() {
        return coveredOutNodes;
    }

    public Set<mxICell> getCoveredInNodes() {
        return coveredInNodes;
    }

    /**
     * @return the total number of covered border nodes in the current ant's path
     */
    public long getCoveredBorderNodesCount() {
        long result = 0;
        // covered IN nodes that are yet in the remainingBorderNodesMap variable
        result += remainingBorderNodesMap.keySet().stream().filter(coveredInNodes::contains).count();
//        result += coveredOutNodes.keySet().stream().filter(coveredIn -> remainingBorderNodesMap.containsKey(coveredIn)).count();
        // covered OUT nodes
        result += coveredOutNodes.values().stream().mapToInt(Set::size).sum();
        return result;
    }

    public int getId() {
        return id;
    }
}