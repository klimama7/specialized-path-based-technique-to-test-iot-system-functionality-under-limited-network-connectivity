package situations_generators.iot_algorithms.ant;

import com.mxgraph.model.mxICell;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import situations_generators.SituationsGeneratorInterface;
import situations_generators.iot_algorithms.GraphTraversalHelper;
import situations_generators.iot_algorithms.LCZGraphUtils;
import situations_generators.iot_algorithms.LimitedConnectivityZone;
import situations_generators.iot_algorithms.TCsGenerationHelper;
import situations_generators.paper_algorithms.measuring.Stopwatches;
import structure.AbstractGraphCell;
import structure.GraphModelCore;
import structure.IoTCoverage;
import structure.TestSituations;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * This class contains the ACO algorithm implementation for finding the test cases in a graph that represents a SUT
 * with a specified limited connection zones.
 */
public class ANTGenerator implements SituationsGeneratorInterface {

    private static final Logger LOG = LogManager.getLogger();

    /**
     * A variable that forces to stop the generation of the test cases.
     */
    protected AtomicBoolean isCanceled = new AtomicBoolean(false);

    /**
     * Model is used for using its methods.
     */
    static GraphModelCore model;

    /**
     * This variable stores the threshold for which  we  create  the  test cases T
     */
    static float level;

    /**
     * Test coverage criterion
     */
    static IoTCoverage coverage;

    /*          The following variables are for the ACO algorithm.      */
    /**
     * A constant representing a weight of the pheromone level in the calculation
     */
    public static final int ALPHA = 1;

    /**
     * A constant representing a weight of the desirability level in the calculation
     */
    public static final int BETA = 3;

    /**
     * A constant representing a weight of the distance to the nearest end in the calculation
     */
    public static final int GAMA = 1;

    public static final int CYCLOMATIC_COMPLEXITY = 15;

    /**
     * According to the paper Ant Colony Optimization: A New Meta-Heuristic the value equals 0.5.
     */
    public static final double PHEROMONE_EVAPORIZATION_COEFFICIENT = 0.5;

    /**
     * According to the paper Ant Colony Optimization: A New Meta-Heuristic the value equals 1.
     */
    public static final double INITIAL_PHEROMONE_LEVEL = 1.0;

    /**
     * Constant connected to the pheromone deposit.
     * According to the paper Ant Colony Optimization: A New Meta-Heuristic the value equals 1.
     */
    public static final double Q = 1;

    /**
     * The more ants the better chance to find the best path (short and with many border nodes)
     */
    public static final int ANTS_COUNT = 30;

    /**
     * Factor, which is multiplied by the value of a longest path to a nearest end in a graph
     * and a result of this multiplication represents the maximal length of a potential test case.
     * This factor can be dynamically changed during the ACO algorithm iteration.
     */
    public static double maxPathDistanceMultiplicationFactor;

    /**
     * Maximal number of times that the ACO algorithm can be repeated, if it doesn't find any path with gain > 0
     */
    public static final int MAX_ACO_REPETITIONS = 10;

    /**
     * Variable that returns the reachable IN nodes for each node in the graph (the IN node itself is reachable from itself)
     */
    static final Map<mxICell, Set<mxICell>> reachableInNodesMap = new HashMap<>();
    /**
     * Variable that returns the reachable OUT nodes for each node in the graph (the OUT node itself is reachable from itself)
     */
    static final Map<mxICell, Set<mxICell>> reachableOutNodesMap = new HashMap<>();

    /**
     * The pheromones levels (tau) for every node in a graph
     */
    static final Map<mxICell, Double> pheromoneLevels = new HashMap<>();

    /**
     * The distances levels (distances to a nearest end node normalized to (0,1>) for every node in the graph
     */
    static final Map<mxICell, Double> distanceLevels = new HashMap<>();

    /**
     * The longest distance from the start node to any of the end nodes
     */
    private static int maxDistance = Integer.MIN_VALUE;

    private final Set<mxICell> endNodes;

    private long counter = 0;

    /**
     * Constructor for finding the test situations bns and lczs.
     *
     * @param model Actual model instance in which the test situations should be found
     * @param level the threshold for which  we  create  the  test cases T
     */
    public ANTGenerator(GraphModelCore model, IoTCoverage coverage, float level) {
        ANTGenerator.model = model;
        ANTGenerator.coverage = coverage;
        ANTGenerator.level = level;
        this.endNodes = GraphTraversalHelper.findEndNodes(model);
        Map<mxICell, Integer> distancesToNearestEnd = GraphTraversalHelper.initDistancesToNearestEnd(model, endNodes);
        initDistanceLevels(distancesToNearestEnd);
    }

    @Override
    public TestSituations generateTestSituations() {
        try {

            LCZGraphUtils lczGraphUtils = new LCZGraphUtils(model, level);
            printLimitedConnectivityZones(lczGraphUtils.getLCZs());

            Stopwatches.getInstance().start();
            Map<Integer, Set<mxICell>> lczInsMap = new HashMap<>();
            Map<Integer, Set<mxICell>> lczOutsMap = new HashMap<>();
            Set<mxICell> borderNodes = new HashSet<>();
            transformLCZs(lczGraphUtils.getLCZs(), lczInsMap, lczOutsMap, borderNodes);

            Map<mxICell, Set<mxICell>> remainingBorderNodesMap = generateReachableBorderNodesMap(lczInsMap, lczOutsMap);

            TestSituations testSituations = generateTSsThroughACO(borderNodes, remainingBorderNodesMap, lczGraphUtils.getLCZs());
            Stopwatches.getInstance().stop();

            return testSituations;

        } catch (Exception e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String stacktrace = sw.toString();
            LOG.error(stacktrace);
            throw e;
        }

    }

    /**
     * Fill variables based on LCZs in current graph.
     *
     * @param limitedConnectivityZones in the graph
     * @param lczInsMap                map of all LCZ IN nodes in the graph
     * @param lczOutsMap               map of all LCZ OUT nodes in the graph
     * @param borderNodes              set of all LCZ border nodes in the graph
     */
    private void transformLCZs(List<LimitedConnectivityZone> limitedConnectivityZones, Map<Integer, Set<mxICell>> lczInsMap,
                               Map<Integer, Set<mxICell>> lczOutsMap, Set<mxICell> borderNodes) {
        for (LimitedConnectivityZone zone : limitedConnectivityZones) {
            lczInsMap.put(zone.getId(), zone.getInNodes());
            lczOutsMap.put(zone.getId(), zone.getOutNodes());
            borderNodes.addAll(zone.getInNodes());
            borderNodes.addAll(zone.getOutNodes());
        }
    }

    /**
     * For each LCZ IN node in graph, find which LCZ OUT nodes are reachable.
     *
     * @param lczInsMap  Map of LCZ IN nodes of each zone
     * @param lczOutsMap Map of LCZ OUT nodes of each zone
     * @return map, where a LCZ IN node is a key and it points to a set of OUT nodes reachable from it.
     */
    private Map<mxICell, Set<mxICell>> generateReachableBorderNodesMap(Map<Integer, Set<mxICell>> lczInsMap,
                                                                       Map<Integer, Set<mxICell>> lczOutsMap) {
        Map<mxICell, Set<mxICell>> borderNodesMap = new HashMap<>();
        Queue<mxICell> queue = new LinkedList<>();
        Set<mxICell> visited = new HashSet<>();
        for (int lczId : lczInsMap.keySet()) {
            for (mxICell lczIn : lczInsMap.get(lczId)) {
                queue.add(lczIn);
                while (!queue.isEmpty()) {
                    mxICell temp = queue.poll();
                    if (lczOutsMap.get(lczId).contains(temp) && (visited.size() > 1 || temp != lczIn)) {
                        // temp is an LCZ OUT node
                        // if IN and OUT are the same, check if possible path exists inside the LCZ
                        Set<mxICell> outNodes = borderNodesMap.getOrDefault(lczIn, new HashSet<>());
                        outNodes.add(temp);
                        borderNodesMap.put(lczIn, outNodes);
                    }
                    for (mxICell outEdge : model.getOutgoingEdgesAsList(temp)) {
                        AbstractGraphCell edgeValue = (AbstractGraphCell) outEdge.getValue();
                        if (edgeValue.getLimitedConnectionProbability() >= level && !visited.contains(outEdge.getTerminal(false))) {
                            queue.add(outEdge.getTerminal(false));
                            visited.add(outEdge.getTerminal(false));
                        }
                    }
                }
                visited.clear();
            }
        }

        return borderNodesMap;
    }

    /**
     * Calls the ACOalgorithm iteratively until all border nodes are not covered in the test situations.
     *
     * @param borderNodes             that should be covered in the test situations
     * @param remainingBorderNodesMap map with remaining border nodes (IN node and points to a set of reachable OUT nodes).
     * @param lczs                    map with limited connectivity zones in the graph.
     * @return the generated set of test situations
     */
    private TestSituations generateTSsThroughACO(final Set<mxICell> borderNodes, Map<mxICell, Set<mxICell>> remainingBorderNodesMap,
                                                 List<LimitedConnectivityZone> lczs) {

        Set<Deque<mxICell>> testSituations = new HashSet<>();
        Set<mxICell> coveredInNodesSet = new HashSet<>();
        int round = 1;

        while (!remainingBorderNodesMap.isEmpty()) {
            if (isCanceled.get()) {
                throw new IllegalStateException("Test cases generation was cancelled by the user");
            }

            // call the ACO algorithm that finds a new test situation
            int ACO_repetition = 1;
            Ant champion = ACOAlgorithm(remainingBorderNodesMap, lczs);
            while (champion == null || champion.getCoveredBorderNodesCount() == 0) {
                LOG.info(round + ": ACO algorithm didn't find any paths that would cover some border nodes. " +
                        "Repetition No: " + ACO_repetition);
                champion = ACOAlgorithm(remainingBorderNodesMap, lczs);
                if (++ACO_repetition > MAX_ACO_REPETITIONS) {
                    throw new IllegalStateException("The ant's champion doesn't cover any border nodes.");
                }
            }
            testSituations.add(champion.getPath());

            // remove border nodes from the remainingBorderNodesMap found in the best path
            LOG.info(round + ": Border nodes before removal ("
                    + getNumberOfRemainingBorderNodes(remainingBorderNodesMap) + "): "
                    + TCsGenerationHelper.printCells(remainingBorderNodesMap));
            // remove the OUT nodes first
            for (mxICell inNode : champion.getCoveredOutNodes().keySet()) {
                for (mxICell outNode : champion.getCoveredOutNodes().get(inNode)) {
                    remainingBorderNodesMap.get(inNode).remove(outNode);
                }
            }
            // now remove the IN nodes
            for (mxICell inNode : champion.getCoveredInNodes()) {
                coveredInNodesSet.add(inNode);
                if (remainingBorderNodesMap.get(inNode).isEmpty()) {
                    remainingBorderNodesMap.remove(inNode);
                }
            }
            if (coverage.isEachInAndOutNodeOnceCoverage()) {
                remainingBorderNodesMap.keySet().removeIf(inNode ->
                        coveredInNodesSet.contains(inNode) && remainingBorderNodesMap.get(inNode).isEmpty());
            }

            LOG.info(round + ": Border nodes after removal ("
                    + getNumberOfRemainingBorderNodes(remainingBorderNodesMap) + "): "
                    + TCsGenerationHelper.printCells(remainingBorderNodesMap));

            LOG.info("////////////////////         END OF ONE ROUND         ////////////////////");
            LOG.info("Test situations in this round:\n" + testSituations.stream().map(ts -> String.valueOf(
                    ts.stream().map(cell -> cell.getValue().toString() + " - ").collect(Collectors.toList()))
                    + "\n").collect(Collectors.toList())
            );
            round++;
        }

        TestSituations reverseTestSituations = TCsGenerationHelper.reverseTestSituations(borderNodes, new LinkedList<>(testSituations));
        LOG.info("Test situations statistics: " + reverseTestSituations.generateOneLinerStatistics());
        LOG.info("Test situations:\n" + reverseTestSituations.testSituationsToString());

        return reverseTestSituations;
    }

    private int getNumberOfRemainingBorderNodes(Map<mxICell, Set<mxICell>> remainingBorderNodesMap) {
        int outNodes = remainingBorderNodesMap.values().stream().mapToInt(Set::size).sum();
        int inNodes = remainingBorderNodesMap.keySet().size();
        return inNodes + outNodes;
    }

    /**
     * Implementation of the ACO algorithm
     *
     * @param remainingBorderNodesInIteration the map with uncovered border nodes (IN nodes in the keys and set of reachable OUT nodes as values)
     * @param lczs                            map with limited connectivity zones in the graph.
     * @return a path that was found by the algorithm
     */
    public Ant ACOAlgorithm(Map<mxICell, Set<mxICell>> remainingBorderNodesInIteration, List<LimitedConnectivityZone> lczs) {

        // declare local variables
        int iteration = 0;

        List<Ant> ants = new ArrayList<>(ANTS_COUNT);

        // init variables
        initDesirabilitiesAsReachableBorderNodes(remainingBorderNodesInIteration);
        printReachableBorderNodes();

        for (mxICell edge : model.getEdges()) {
            pheromoneLevels.put(edge, INITIAL_PHEROMONE_LEVEL);
        }
        for (int i = 0; i < ANTS_COUNT; i++) {
            ants.add(new Ant(model.getStart(), i, lczs, remainingBorderNodesInIteration));
        }

        Ant champion = null;

        // repeat the algorithm number of times equal to the cyclomatic complexity
        while (iteration <= CYCLOMATIC_COMPLEXITY) {
            maxPathDistanceMultiplicationFactor = 1;

            // iterate the ants until all of them doesn't reach the end
            List<Ant> tempAnts = new ArrayList<>(ants);
            Iterator<Ant> antIterator = tempAnts.iterator();
            Ant a = antIterator.next();

            while (true) {
                LOG.trace("Ant " + a + " starts its search for a path");
                if (iteration > 0) {
                    a.reset();
                }

                // Traverse the graph with current ant
                while (!a.endReached() && a.getCost() <= maxDistance * maxPathDistanceMultiplicationFactor) {
//                    a.printCurrentDesirabilities();
                    a.move();
                    a.coverBorderNodes();
                    if (isCanceled.get()) {
                        throw new IllegalStateException("Test cases generation was cancelled by the user");
                    }
                }
                if (a.endReached()) {
                    LOG.debug("Ant " + a + " found a path (" + a.getCost() + ", " + a.getCoveredBorderNodesCount() + ")"
                            + LCZGraphUtils.mxICellListToString(a.getPath(), " - "));
                    antIterator.remove();
                    if (antIterator.hasNext()) {
                        a = antIterator.next();
                    } else {
                        break;
                    }
                } else { // The length of the current ant's path reached the actual limit
                    LOG.debug("Ant " + a + " was trapped in a cycle and restarts its search " +
                            "(maxDistance: " + maxDistance + ", maxPathDistanceMultiplicationFactor: " + maxPathDistanceMultiplicationFactor);
                    a.reset();
                    // For every tenth ant double the max path distance multiplication factor
//                    if ((a.getId() % 10) == 0) {
                    antIterator.remove();
                    if (antIterator.hasNext()) {
                        maxPathDistanceMultiplicationFactor *= 1.2;
                        a = antIterator.next();
                    } else {
                        break;
                    }
                }
            }

            final Set<Ant> champions = findChampions(ants);
            if (champions != null) {
                if (champion != null) {
                    Ant newChampion = champions.iterator().next();
                    if (champion.getCoveredBorderNodesCount() > newChampion.getCoveredBorderNodesCount()) {
                        LOG.debug("using champion from previous iterations " + champion);
                        champions.clear();
                        champions.add(champion);
                    } else if (champion.getCoveredBorderNodesCount() == newChampion.getCoveredBorderNodesCount()
                            && champion.getCost() < newChampion.getCost()) {
                        LOG.debug("using champion from previous iterations " + champion);
                        champions.clear();
                        champions.add(champion);
                    }
                }

                // Update pheromone levels for the best path
                LOG.info("Iteration#" + iteration + " - " + champions.size() + " champion(s): ");
                for (Ant ant : champions) {
                    Set<mxICell> edgesInPathMap = new HashSet<>();
                    ant.printPath("\t" + ant + ": ", Level.DEBUG);
                    if (ant.getCoveredBorderNodesCount() > 0) {
                        for (mxICell e : ant.getPath()) {
                            if (!edgesInPathMap.contains(e)) {
                                pheromoneLevels.put(e, pheromoneLevels.get(e) + (Q / ant.getCost())); // equation (3) from ACO: A New Meta-Heuristic
                            }
                            edgesInPathMap.add(e);
                        }
                    }
                }
                // update the pheromone levels for each edge
                for (mxICell e : model.getEdges()) {
                    pheromoneLevels.put(e, ((1 - PHEROMONE_EVAPORIZATION_COEFFICIENT) * pheromoneLevels.get(e))); // equation (4) from ACO: A New Meta-Heuristic
                }
                LOG.debug("NEW Pheromone levels: ");
                LOG.debug(pheromoneLevels.keySet().stream().map(key -> key.getValue().toString() + "=" + pheromoneLevels.get(key)).collect(Collectors.toList()));
                champion = new Ant(champions.iterator().next());
                LOG.debug("---- END OF ITERATION Iteration#" + iteration + " - champion: ");
            } else {
                LOG.debug("---- END OF ITERATION No path found in iteration#" + iteration + " - champion: ");
            }
            iteration++;
            if (champion != null) {
                champion.printPath("\t" + champion + ": ", Level.DEBUG);
            } else {
                LOG.debug("No champion found yet");
            }
        }
        LOG.info("---- END OF ACO Algorithm ---");
        if (champion != null) {
            champion.printPath("\tchampion: " + champion + ": ", Level.INFO);
        } else {
            LOG.info("No champion found yet");
        }
        return champion;

    }

    /**
     * Find a set of ants that found the best path (with the biggest number of border nodes and the smallest cost)
     *
     * @param ants that are iterated and those with the best path are saved to the resulted set
     * @return set with ants with the best path
     */
    private Set<Ant> findChampions(List<Ant> ants) {

        // Find ant(s) with the best path
        final Set<Ant> champions = new HashSet<>();
        champions.add(ants.get(0));
        for (Ant a : ants) {
//            if (a.endReached()) {
                final Ant oldChamp = champions.iterator().next();
                if (a.getCoveredBorderNodesCount() > oldChamp.getCoveredBorderNodesCount()) {
                    champions.clear();
                    champions.add(a);
                } else if (a.getCoveredBorderNodesCount() == oldChamp.getCoveredBorderNodesCount()) {
                    if (a.getCost() < oldChamp.getCost()) {
                        champions.clear();
                        champions.add(a);
                    } else if (a.getCost() == oldChamp.getCost()) {
                        champions.add(a);
                    }
                }
//            }
        }

        if (champions.iterator().next().getCoveredBorderNodesCount() == 0) {
            return null;
        }

        return champions;

    }


    /**
     * Assign the distance levels to the edges according to the distance to the nearest end.
     *
     * @param distancesToNearestEnd map with the distances to the nearest end node from each node.
     */
    private void initDistanceLevels(Map<mxICell, Integer> distancesToNearestEnd) {
        for (mxICell node : model.getNodes()) {
            double sumOfDistances = 0; // sum of edges needed to reach the end node from current node
            if (model.getOutgoingEdgesAsList(node).size() > 1) {
                for (mxICell edge : model.getOutgoingEdgesAsList(node)) {
                    sumOfDistances += distancesToNearestEnd.get(edge.getTerminal(false)) + 1;
                }
                double sumForNormalization = 0;
                for (mxICell edge : model.getOutgoingEdgesAsList(node)) { // TODO MAKE MATHEMATICALLY CLEAR
                    sumForNormalization += 1 - ((distancesToNearestEnd.get(edge.getTerminal(false)) + 1) / sumOfDistances);
                }
                for (mxICell edge : model.getOutgoingEdgesAsList(node)) {
                    double distanceLevel = (1 - ((distancesToNearestEnd.get(edge.getTerminal(false)) + 1) / sumOfDistances)) / sumForNormalization;
                    distanceLevels.put(edge, distanceLevel);
                }
            } else {
                for (mxICell edge : model.getOutgoingEdgesAsList(node)) {
                    distanceLevels.put(edge, 1.0);
                }
            }
        }
    }


    /**
     * Find all end nodes in the graph and traverse from them to the start node and count how many border nodes
     * there are on the way. Then, assign the desirabilities to the edges according to the number of border nodes
     * reachable from the target node.
     *
     * @param remainingBorderNodesMap the map with the remaining border nodes (IN nodes in the keys and set of reachable OUT nodes as values)
     */
    private void initDesirabilitiesAsReachableBorderNodes(Map<mxICell, Set<mxICell>> remainingBorderNodesMap) {

        counter = 0;
        reachableInNodesMap.clear();
        reachableOutNodesMap.clear();

        // from each of the end nodes traverse the graph and find which border nodes are reachable from which nodes
        for (mxICell end : endNodes) {
            Set<mxICell> reachableOutNodes = reachableOutNodesMap.getOrDefault(end, new HashSet<>());
            boolean targetIsOutNode = remainingBorderNodesMap.values().stream().anyMatch((set) -> set.contains(end));
            if (targetIsOutNode) { // Add the target node itself only if the edge is in the zone
                reachableOutNodes.add(end);
            }
            reachableOutNodesMap.put(end, reachableOutNodes);
            reachableInNodesMap.put(end, new HashSet<>());
            for (mxICell edge : model.getIncomingEdgesAsList(end)) {
                Set<mxICell> visited = new HashSet<>();
                generateReachableBorderNodesMap(remainingBorderNodesMap, edge, visited, 1);
            }
        }

        // make LCZ IN nodes to reach the nodes itself
        remainingBorderNodesMap.keySet().forEach(in -> reachableInNodesMap.get(in).add(in));
    }

    /**
     * Recursive function that begins in the end node and then continues to the parents and saves the number
     * of border nodes visited on the way. It also ensures that from the OUT nodes the reachability is copied only
     * on the way through the LCZ and that it doesn't by-pass it.
     * <p>
     * Every newly visited node should copy all of the border nodes reachable from the target node itself,
     * and in the following situations even the <b>target node itself should be added to the list of reachable border nodes<b/>:
     * <p>
     * The resulted collection of border nodes is stored in the class variables
     * {@link ANTGenerator#reachableInNodesMap} and {@link ANTGenerator#reachableOutNodesMap}
     * Also assign the {@link ANTGenerator#maxDistance} variable to the biggest number.
     *
     * @param remainingMap    the map with the remaining border nodes (IN nodes in the keys and set of reachable OUT nodes as values)
     * @param edge            currently traversed edge
     * @param visited         visited edges on the way so that the algorithm doesn't end up in the infinite loop
     * @param distanceToStart counts the distance from end nodes to start node
     */
    private void generateReachableBorderNodesMap(Map<mxICell, Set<mxICell>> remainingMap,
                                                 mxICell edge, Set<mxICell> visited, int distanceToStart) {
        // edit the sets of nodes reachable from the source, so that it contains the nodes reachable from the target
        if (!visited.contains(edge)) {
            AbstractGraphCell edgeValue = (AbstractGraphCell) edge.getValue();
            counter++;
            if (maxDistance <= distanceToStart) {
                maxDistance = distanceToStart;
            }

            mxICell source = edge.getTerminal(true);
            mxICell target = edge.getTerminal(false);
            // fetch or create the set of nodes that are reachable from the source node
            Set<mxICell> reachableInNodes = reachableInNodesMap.getOrDefault(source, new HashSet<>());
            Set<mxICell> reachableOutNodes = reachableOutNodesMap.getOrDefault(source, new HashSet<>());
            boolean targetIsInNode = remainingMap.containsKey(target) && edgeValue.getLimitedConnectionProbability() < level;
            boolean targetIsOutNode = edgeValue.getLimitedConnectionProbability() >= level
                    && remainingMap.values().stream().anyMatch((set) -> set.contains(target));

            // create a new copy of the visited nodes list so that it won't end up in the infinite loop
//            Set<mxICell> copyOfVisited = new HashSet<>(visited);
            // add the source edge to the list of the visited edges
//            copyOfVisited.add(edge);
            visited.add(edge);
            // copy the information of reachable nodes from the target
            reachableInNodes.addAll(reachableInNodesMap.get(target));
            reachableOutNodes.addAll(reachableOutNodesMap.get(target));

            if (targetIsInNode) { // Add the target node only if it is the LCZ IN node
                reachableInNodes.add(target);
            }
            if (targetIsOutNode) { // Add the target node if it is the LCZ OUT node
                reachableOutNodes.add(target);
            }
            reachableInNodesMap.put(source, reachableInNodes);
            reachableOutNodesMap.put(source, reachableOutNodes);

            for (mxICell inEdge : model.getIncomingEdgesAsList(source)) {
                generateReachableBorderNodesMap(remainingMap, inEdge, visited, distanceToStart + 1);
            }
        }
    }

    @SuppressWarnings("unused")
    private void printNodes(Deque<mxICell> endNodes, String description) {
        LOG.info(description);
        StringBuilder endNodesString = new StringBuilder();
        endNodes.forEach((e) -> endNodesString.append(e.getValue()).append(", "));
        LOG.info(endNodesString.toString());
    }

    @SuppressWarnings("unused")
    private void printPheromoneLevels() {
        StringBuilder sb = new StringBuilder();
        LinkedList<mxICell> sortedEdges = new LinkedList<>(pheromoneLevels.keySet());
        sortedEdges.sort(Comparator.comparing(o -> Integer.parseInt(o.getValue().toString())));
        for (mxICell edge : sortedEdges) {
            sb.append(edge.getValue()).append(": ").append(pheromoneLevels.get(edge)).append(", ");
        }
        LOG.info("Pheromone levels: ");
        LOG.info(sb.toString());
    }

    private void printReachableBorderNodes() {
        StringBuilder sb = new StringBuilder();
        LinkedList<mxICell> sortedNodes = new LinkedList<>(reachableInNodesMap.keySet());
        sortedNodes.sort(Comparator.comparing(o -> o.getValue().toString()));
        for (mxICell node : sortedNodes) {
            sb.append(node.getValue()).append(": ")
                    .append("in").append(reachableInNodesMap.get(node).stream().map(n -> n.getValue().toString()).collect(Collectors.toList()))
                    .append("out").append(reachableOutNodesMap.get(node).stream().map(n -> n.getValue().toString()).collect(Collectors.toList()))
                    .append(", ");
        }
        LOG.debug("Desirability levels: ");
        LOG.debug(counter + ": " + sb);
    }

    private void printLimitedConnectivityZones(List<LimitedConnectivityZone> limitedConnectivityZones) {
        for (LimitedConnectivityZone zone : limitedConnectivityZones) {
            LOG.info("Printing zone ID: " + zone.getId());
            LOG.info(zone);
        }
    }

    @Override
    public void setCancelled() {
        isCanceled.set(true);
    }

}
