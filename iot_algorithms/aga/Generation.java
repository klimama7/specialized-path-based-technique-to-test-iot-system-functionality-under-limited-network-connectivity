package situations_generators.iot_algorithms.aga;

import com.mxgraph.model.mxICell;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import situations_generators.iot_algorithms.LCZGraphUtils;
import structure.GraphModelCore;
import structure.IoTCoverage;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Generation {

    private static final Logger LOG = LogManager.getLogger();

    private final int generationNumber;

    private final int originalChromosomesSize;

    private final List<Chromosome> chromosomes;

    private final GraphModelCore model;

    private final Map<mxICell, Set<mxICell>> remainingBorderNodesMap;

    private final IoTCoverage coverage;

    private final float threshold;

    /**
     * Generate the initial generation of the chromosomes
     *
     * @param chromosomesCount        that should be initialized
     * @param startEdges              edges outgoing from start node of graph
     * @param endEdges                edges incoming the end nodes in the graph
     * @param remainingBorderNodesMap That is changed when some test case is found
     * @param coverage                To know, how to handle the covering border nodes
     * @param threshold               To be able to generate the LCZs.
     */
    public Generation(int chromosomesCount, List<mxICell> startEdges, List<mxICell> endEdges, GraphModelCore model, Map<mxICell, Set<mxICell>> remainingBorderNodesMap, IoTCoverage coverage, float threshold) {
        this.generationNumber = 1;
        this.chromosomes = new ArrayList<>();
        this.originalChromosomesSize = chromosomesCount;
        this.model = model;
        this.remainingBorderNodesMap = remainingBorderNodesMap;
        this.coverage = coverage;
        this.threshold = threshold;
        initInitialGeneration(startEdges, endEdges, chromosomesCount, false);
    }

    /**
     * Generate new generation from a predefined list of chromosomes
     *
     * @param generationNumber        that describes the state of evolution of the generation
     * @param chromosomes             to be used in the generation
     * @param model                   of graph
     * @param remainingBorderNodesMap That is changed when some test case is found
     * @param coverage                To know, how to handle the covering border nodes
     * @param threshold               To be able to generate the LCZs.
     */
    public Generation(int generationNumber, List<Chromosome> chromosomes, GraphModelCore model, Map<mxICell, Set<mxICell>> remainingBorderNodesMap, IoTCoverage coverage, float threshold) {
        this.generationNumber = generationNumber;
        this.chromosomes = chromosomes;
        this.originalChromosomesSize = chromosomes.size();
        this.model = model;
        this.remainingBorderNodesMap = remainingBorderNodesMap;
        this.coverage = coverage;
        this.threshold = threshold;
    }

    /**
     * Initialize the set of chromosomes (the initial round of the algorithm).
     * The chromosomes will start in the start node of the graph and end in one of the end nodes, selected randomly.
     */
    private void initInitialGeneration(List<mxICell> startEdges, List<mxICell> endEdges, int chromosomesCount, boolean addEndEdges) {
        //TODO Try different way to generate the initial population (e.g. all nodes / only start node)
        while (chromosomesCount > 0) {
            for (mxICell startEdge : startEdges) {
                for (mxICell endEdge : endEdges) {
                    List<mxICell> path = new ArrayList<>();
                    path.add(startEdge);
                    if (addEndEdges) {
                        path.add(endEdge);
                    }
                    Chromosome newChromosome = new Chromosome(path, model, remainingBorderNodesMap, coverage, threshold);
                    if (!addEndEdges) {
                        newChromosome.breeding();
                    }
                    chromosomes.add(newChromosome);
                    chromosomesCount--;
                    if (chromosomesCount == 0) {
                        return;
                    }
                }
            }
        }
    }

    /**
     * Evaluate the fitness of chromosomes and select the parents of offsprings.
     * It uses the roulette wheel method
     * -> chromosomesCount times execute the selection with probability equal to fitness of individual / total fitness
     *
     * @return the parents of next generation
     */
    private List<Chromosome> parentsSelection() {
        double totalFitness = 0;
        for (Chromosome ch : chromosomes) {
            if (ch.getPath().size() > 1) { // otherwise the whole path is made from one edge that goes from start to end
                totalFitness += ch.getFitness();
            }
        }

        double sumNormalizedFitness = 0;
        final Map<Chromosome, Double> chromosomesNormalizedFitnessMap = new TreeMap<>();
        Map<Chromosome, Integer> chromosomeCounterMap = new HashMap<>();
        // normalize fitness in collection
        for (Chromosome ch : chromosomes) {
            if (ch.getPath().size() > 1) { // otherwise the whole path is made from one edge that goes from start to end
                double normalizedFitness = ch.getFitness() / totalFitness;
                if (chromosomesNormalizedFitnessMap.containsKey(ch)) { // there are duplicates in the chromosomes
                    int chromosomeCount = chromosomeCounterMap.getOrDefault(ch, 1);
                    chromosomeCounterMap.put(ch, ++chromosomeCount); // increment chromosome counter
                }
                sumNormalizedFitness += normalizedFitness;
                chromosomesNormalizedFitnessMap.put(ch, normalizedFitness);
            }
        }

        List<Chromosome> parents = new ArrayList<>();

        double randomNum = -1;
        double accumulatedFitness = 0;
        try {
            for (int i = 0; i < originalChromosomesSize; i++) {
                Chromosome chromosome = null;
                accumulatedFitness = 0;

                randomNum = ThreadLocalRandom.current().nextDouble(sumNormalizedFitness);
                Iterator<Chromosome> chromosomesIterator = chromosomesNormalizedFitnessMap.keySet().iterator();
                while (accumulatedFitness < randomNum) {
                    chromosome = chromosomesIterator.next();
                    // if there are duplicates in the original chromosomes list, repeat the fitness addition
                    for (int j = 0; j < chromosomeCounterMap.getOrDefault(chromosome, 1); j++) {
                        accumulatedFitness += chromosomesNormalizedFitnessMap.get(chromosome);
                    }
                }
                parents.add(chromosome);
            }
        } catch (Exception e) {
            LOG.error("Error in parents selection. Random num: " + randomNum
                    + " sum normalized fitness: " + sumNormalizedFitness
                    + " accumulated fitness: " + accumulatedFitness);
            e.printStackTrace();
        }
        if (parents.size() != originalChromosomesSize) {
            throw new IllegalStateException("Set of parents chromosomes has a different size from the original list of chromosomes.");
        }

        return parents;
    }

    /**
     * Generate a list of offsprings that were made by a crossover of parents chromosomes with the crossoverProbability
     *
     * @param crossoverProbability that the crossover takes place
     * @param parents              that should make offsprings
     * @return a list of offsprings that came from parents
     */
    private List<Chromosome> crossovers(List<Chromosome> parents, double crossoverProbability) {
        Random random = new Random();
        List<Chromosome> offsprings = new ArrayList<>();
        int i = 0;
        while (offsprings.size() < originalChromosomesSize) {
//        for (int i = 0; i < parents.size() - 1; i += 2) {
            // Find parent and partner (the first distinct pair of chromosomes with path longer than 1)
            Chromosome parent = null;
            Chromosome partner = null;
            parent = parents.get(i % parents.size());
            while (parent.getPath().size() < 2) {
                parent = parents.get(++i % parents.size());
            }
            partner = parents.get(++i % parents.size());
            while (parent == partner || partner.getPath().size() < 2) {
                partner = parents.get(++i % parents.size());
            }
            double doCrossover = Math.random();
            if (doCrossover < crossoverProbability) { // insert the crossover of parent and partner to offsprings
                int shorterParentLength = Math.min(parent.getLength(), partner.getLength());
                int crossingPoint = 1 + random.nextInt(shorterParentLength - 1);
                offsprings.add(parent.leftCrossover(partner, crossingPoint));
                offsprings.add(partner.leftCrossover(parent, crossingPoint));
            } else {  // insert the parent and partner themselves to offsprings
                offsprings.add(new Chromosome(parent.getPath(), model, remainingBorderNodesMap, coverage, threshold));
                offsprings.add(new Chromosome(partner.getPath(), model, remainingBorderNodesMap, coverage, threshold));
            }
        }
        return offsprings;
    }

    /**
     * Generate offsprings a selection and reproduction of the current generation.
     *
     * @param geneMutationProbability by which the genes of a chromosome should be mutated
     * @param chromosomeMutationProbability by which the mutation of a chromosome should happen
     * @param crossoverProbability by which the crossover of chromosomes should happen
     * @return offsprings that came from this generation
     * @throws Exception when the selection or reproduction didn't succeed
     */
    public List<Chromosome> selectionAndReproduction(double geneMutationProbability, double chromosomeMutationProbability, double crossoverProbability) throws Exception {
        List<Chromosome> parents;
        List<Chromosome> offsprings;
        if (generationNumber > 1) {
            parents = parentsSelection(); // find parents
            offsprings = crossovers(parents, crossoverProbability); // crossover parents
        } else {
            offsprings = crossovers(this.chromosomes, crossoverProbability);
        }
//        double offspringsSumAdjacencyFitness = Generation.sumAdjacencyQualityFitnessOfChromosomes(offsprings);
        for (Chromosome offspring : offsprings) {
//            offspring.getFitness(offspringsSumAdjacencyFitness);
            offspring.getFitness();
        }
        offsprings.sort(Comparator.comparing(Chromosome::getFitness).reversed()); // sort the offsprings
//        while (offsprings.size() > chromosomesCount) { // let there be only chromosomesCount offsprings; remove others
//            offsprings.remove(offsprings.size() - 1);
//        }
        for (Chromosome ch : offsprings) {
            double doMutation = Math.random();
            if (doMutation < chromosomeMutationProbability) {
                ch.mutation(geneMutationProbability);
            }
        }
        return offsprings;
    }


    public int getChromosomesSize() {
        return chromosomes.size();
    }

    public int getOriginalChromosomesSize() {
        return originalChromosomesSize;
    }

    /**
     * Enhance the population in nextGeneration by the best member of the current population
     * <p>
     * !IMPORTANT! The implementation is different from the one in the paper,
     * where the exchange takes place when best in next is worse than best in this
     *
     * @param nextGeneration to be enhanced by the member of the current population
     */
    public void elitist(List<Chromosome> nextGeneration) {
        Chromosome bestInThisNotInNext = findBestOrWorstNotInNext(true, this.chromosomes, nextGeneration);
        Chromosome worstInNext = findBestOrWorst(false, nextGeneration);
        if (bestInThisNotInNext != null) {
            if (bestInThisNotInNext.getFitness() > worstInNext.getFitness()) {
                int worstInNextIndex = nextGeneration.indexOf(worstInNext);
                nextGeneration.set(worstInNextIndex, bestInThisNotInNext);
            }
        }
    }


    /**
     * If there are some empty spaces in the next population, put there the random members from the current population.
     *
     * @param nextGeneration to be enhanced by the member of the current population
     */
    public void elitistToEqualTheSize(List<Chromosome> nextGeneration) {
//        TreeSet<Chromosome> sortedChromosomes = new TreeSet<>(chromosomes);
        List<Chromosome> shuffledChromosomes = new ArrayList<>(chromosomes);
        Collections.shuffle(shuffledChromosomes);
        while (nextGeneration.size() < originalChromosomesSize) {
//            Chromosome first = sortedChromosomes.pollFirst();
            Chromosome first = shuffledChromosomes.stream().findFirst().orElse(null);
            shuffledChromosomes.remove(first);
            if (first == null) {
                throw new IllegalStateException();
            } else {
                if (first.getPath().size() > 1) { // Other chromosomes than those with one edge that goes from start to end
                    nextGeneration.add(first);
                }
            }
        }
    }

    /**
     * If there are some empty spaces in the next population, put there the random members from the current population.
     *
     * @param nextGeneration to be enhanced by the member of the current population
     */
    public void initialChromosomesToEqualTheSize(List<Chromosome> nextGeneration) {
        List<mxICell> startEdges = new ArrayList<>(model.getOutgoingEdgesAsList(model.getStart()));
        while (nextGeneration.size() < originalChromosomesSize) {
            for (mxICell startEdge : startEdges) {
                List<mxICell> path = new ArrayList<>();
                path.add(startEdge);
                Chromosome newChromosome = new Chromosome(path, model, remainingBorderNodesMap, coverage, threshold);
                newChromosome.breeding();
                nextGeneration.add(newChromosome);
                if (nextGeneration.size() == originalChromosomesSize) {
                    return;
                }
            }
        }
    }

    public static double sumAdjacencyQualityFitnessOfChromosomes(List<Chromosome> chromosomes) {
        double sum = 0;
        for (Chromosome ch : chromosomes) {
            sum += Math.pow(ch.getAdjacencyFitness(), AGAGenerator.ALPHA)
                    * Math.pow(ch.getQualityFitness(), AGAGenerator.BETA);
        }
        return sum;
    }

    public double sumAdjacencyQualityFitnessOfChromosomes() {
        return sumAdjacencyQualityFitnessOfChromosomes(chromosomes);
    }

    /**
     * @param best           true if the best chromosome should be found, false if the worst chromosome
     * @param chromosomeList where the champion or looser should be found
     * @return the best or worst chromosome from the chromosomeList
     */
    public static Chromosome findBestOrWorst(boolean best, List<Chromosome> chromosomeList) {
        Iterator<Chromosome> chromosomeIterator = chromosomeList.iterator();
        Chromosome bestOrWorst = chromosomeIterator.next();
        Chromosome temp;
        while (chromosomeIterator.hasNext()) {
            temp = chromosomeIterator.next();
            if (best) {
                if (bestOrWorst.getFitness() < temp.getFitness()) {
                    bestOrWorst = temp;
                }
            } else {
                if (bestOrWorst.getFitness() > temp.getFitness()) {
                    bestOrWorst = temp;
                }
            }
        }
        return bestOrWorst;
    }

    /**
     * @param best       true if the best chromosome should be found, false if the worst chromosome
     * @param sourceList where the champion or looser should be found
     * @param nextList   where the champion or looser should NOT be present
     * @return the best or worst chromosome from the chromosomeList
     */
    public static Chromosome findBestOrWorstNotInNext(boolean best, List<Chromosome> sourceList, List<Chromosome> nextList) {
        Chromosome bestOrWorst = null;
        for (Chromosome ch : sourceList) {
            if (!nextList.contains(ch)) {
                bestOrWorst = ch;
            }
        }
        if (bestOrWorst == null) {
            return null; // the nextList contains only the chromosomes in sourceList
        }
        Iterator<Chromosome> chromosomeIterator = sourceList.iterator();
        Chromosome temp;
        while (chromosomeIterator.hasNext()) {
            temp = chromosomeIterator.next();
            if (!nextList.contains(temp)) {
                if (best) {
                    if (bestOrWorst.getFitness() < temp.getFitness()) {
                        bestOrWorst = temp;
                    }
                } else {
                    if (bestOrWorst.getFitness() > temp.getFitness()) {
                        bestOrWorst = temp;
                    }
                }
            }
        }
        return bestOrWorst;
    }

    /**
     * Iterate the chromosomes and find whether some represent a valid test case. If it does, cover the border nodes.
     *
     * @param testSituations A final set of test situations
     */
    public boolean handleTCsInGeneration(Set<Deque<mxICell>> testSituations) {
        chromosomes.sort(Comparator.comparing(Chromosome::getFitness).reversed());
        Iterator<Chromosome> chromosomesIterator = chromosomes.iterator();
        Chromosome bestChromosome = null;
        while (chromosomesIterator.hasNext()) {
            Chromosome tempChromosome = chromosomesIterator.next();
            if (tempChromosome.getAdjacencyFitness() == 1.0
                    && tempChromosome.getQualityFitness() > 0
                    && tempChromosome.pathReachesEnd()) {
                bestChromosome = tempChromosome;
                break;
            }
        }
        if (bestChromosome != null) {
            LOG.info("\t Checking chromosome: " + bestChromosome);
            int borderNodesCovered = bestChromosome.borderNodesCover(true);
            LOG.info("\t\tTC Found (" + borderNodesCovered + "): "
                    + LCZGraphUtils.mxICellListToString(bestChromosome.getPath(), " - "));
            LOG.info("\t\tRemaining border nodes: ");
            for (mxICell key : remainingBorderNodesMap.keySet()) {
                LOG.info("\t\t\tFor key " + key.getValue().toString() + ": values " +
                        remainingBorderNodesMap.get(key).stream().map(out -> out.getValue().toString()).collect(Collectors.toList()));
            }
//            LOG.info(chromosomes);
            testSituations.add(new LinkedList<>(bestChromosome.getPath()));
            return true;
        }
//        chromosomeIterator.remove();
        return false;
    }

    /**
     * @return a sub list of the {@link #chromosomes} that contain some edges with adjacent duplicate edges
     */
    public List<Chromosome> getChromosomesWithDuplicateAdjacentEdges() {
        return getChromosomesWithDuplicateAdjacentEdges(chromosomes);
    }

    /**
     * @param originalChromosomesList where the duplicities should be found
     * @return a list of chromosomes that contain some edges with adjacent duplicate edges.
     */
    public static List<Chromosome> getChromosomesWithDuplicateAdjacentEdges(List<Chromosome> originalChromosomesList) {
        List<Chromosome> chromosomesWithDuplicities = new ArrayList<>();
        for (Chromosome ch : originalChromosomesList) {
            if (ch.containDuplicateAdjacentEdges()) {
                chromosomesWithDuplicities.add(ch);
            }
        }
        return chromosomesWithDuplicities;
    }

    public int getGenerationNumber() {
        return generationNumber;
    }

    public double getAverageFitness() {
        double totalFitness = 0;
        for (Chromosome ch : chromosomes) {
            totalFitness += ch.getFitness();
        }
        return totalFitness / originalChromosomesSize;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Generation number: ").append(generationNumber).append("\n");
        chromosomes.sort(Comparator.comparing(Chromosome::getFitness).reversed());
        chromosomes.forEach(ch -> sb.append(ch.toString()).append("\n"));
        sb.append("size: ").append(getChromosomesSize()).append("\n");
        sb.append("avg. fitness is: ").append(getAverageFitness());
        return sb.toString();
    }

    public void complementWithInitialChromosomes() {
        List<mxICell> startEdges = new ArrayList<>(model.getOutgoingEdgesAsList(model.getStart()));
        while (true) {
            for (mxICell startEdge : startEdges) {
                List<mxICell> path = new ArrayList<>();
                path.add(startEdge);
                Chromosome newChromosome = new Chromosome(path, model, remainingBorderNodesMap, coverage, threshold);
                newChromosome.breeding();
                chromosomes.add(newChromosome);
                if (this.chromosomes.size() >= originalChromosomesSize) {
                    return;
                }
            }
        }
    }
}
