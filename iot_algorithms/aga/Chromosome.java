package situations_generators.iot_algorithms.aga;

import com.mxgraph.model.mxICell;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import situations_generators.iot_algorithms.LCZGraphUtils;
import structure.AbstractGraphCell;
import structure.GraphModelCore;
import structure.IoTCoverage;

import java.util.*;

public class Chromosome implements Comparable<Chromosome> {

    private final long id;

    private static long count;

    private static final Logger LOG = LogManager.getLogger();

    /**
     * Deque of edges that together makes the test case
     */
    private final List<mxICell> path;

    private final Random rand = new Random();

    private double fitness;

    private double adjacencyFitness;

    private double qualityFitness;

    private double lczEdgesFitness;

    private double lengthFitness;

    private final GraphModelCore model;

    private final Map<mxICell, Set<mxICell>> remainingBorderNodesMap;

    private final IoTCoverage coverage;

    private final float threshold;

    /**
     * @param path                    Sequence of edges in the chromosome
     * @param model                   Model of the SUT
     * @param remainingBorderNodesMap That is changed when some test case is found
     * @param coverage                To know, how to handle the covering border nodes
     * @param threshold               To be able to generate the LCZs.
     */
    public Chromosome(List<mxICell> path, GraphModelCore model, Map<mxICell, Set<mxICell>> remainingBorderNodesMap, IoTCoverage coverage, float threshold) {
        this.id = ++count;
        this.path = new ArrayList<>(path);
        this.model = model;
        this.remainingBorderNodesMap = remainingBorderNodesMap;
        this.coverage = coverage;
        this.threshold = threshold;
        this.resetFitness();
//        calculateFitness();
    }

    /**
     * Change each cell in the chromosome with the mutation probability
     *
     * @param mutationProbability expected number of mutated cells in the chromosome
     */
    public void mutation(double mutationProbability) throws Exception {
        int pathLength = path.size();
        try {
            for (int i = 0; i < path.size(); i++) {
                mxICell edge = path.get(i);
                double randomNum = Math.random();
                if (randomNum < mutationProbability) {
                    mxICell source = edge.getTerminal(true);
                    try {
                        mxICell newEdge = getRandomOutgoingEdgeFromNode(source);
                        path.set(i, newEdge);
                    } catch (Exception e) {
                        LOG.debug(e.getMessage());
                    }
                }
            }
            this.resetFitness();
//            calculateFitness();
        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new Exception("Unable to mutate chromosome " + this);
        }
        if (path.size() != pathLength) {
            throw new Exception("Mutation changed length of chromosome " + this);
        }
    }

    /**
     * Extend the chromosome by one edge in the not adjacent part of the path to represent a complete path.
     */
    public void breeding() {
        int originalPathLength = path.size();
        try {
            Iterator<mxICell> pathIterator = path.iterator();
            mxICell previous = pathIterator.next();
            mxICell edge;
            int breedingEdgeIdx = 0;
            while(pathIterator.hasNext()) {
                edge = pathIterator.next();
                if (previous.getTerminal(false).equals(edge.getTerminal(true))) { // edges are connected
                    // the edges are different and connected
                    breedingEdgeIdx++;
                } else if (model.getOutgoingEdgesAsList(previous.getTerminal(false)).isEmpty()){ // disconnected by end node in the middle of the path
                    breedingEdgeIdx++;
                } else { // disconnected otherwise - break from the loop
                    break;
                }
                previous = edge;
            }
            if (breedingEdgeIdx < path.size()) {
                mxICell parentNode = path.get(breedingEdgeIdx).getTerminal(false);
                if (!model.getOutgoingEdgesAsList(parentNode).isEmpty()) {
                    mxICell newBornEdge = getRandomOutgoingEdgeFromNode(parentNode);
                    if (newBornEdge != null) {
                        path.add(breedingEdgeIdx + 1, newBornEdge);
                        this.resetFitness();
                    }
                }
            } else {
                throw new Exception("Breeding unsuccessful, the path is made from connected paths: " + LCZGraphUtils.mxICellListToString(path, " - "));
            }
//            calculateFitness();
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
//        double maxAdjacentEdgesCount = 1;
//        double adjacentEdgesCount = 1;
//        int tempIndex = 0;
//        int indexOfEnd = 0;
//        Iterator<mxICell> pathIterator = path.iterator();
//        mxICell previous = pathIterator.next();
//        mxICell edge;
//        while(pathIterator.hasNext()) {
//            edge = pathIterator.next();
//            tempIndex++;
//            if (!previous.equals(edge)
//                    && previous.getTerminal(false).equals(edge.getTerminal(true))) {
//                // the edges are different and connected
//                adjacentEdgesCount++;
//            } else {
//                adjacentEdgesCount = 1;
//            }
//            if (maxAdjacentEdgesCount < adjacentEdgesCount) {
//                maxAdjacentEdgesCount = adjacentEdgesCount;
//                indexOfEnd = tempIndex;
//            }
//            previous = edge;
//        }
//
//        int pathLength = path.size();
//        try {
//            int breedingEdgeIdx = indexOfEnd;
//            mxICell maxAdjacentPathLastNode = path.get(breedingEdgeIdx).getTerminal(false);
//            while (model.getOutgoingEdgesAsList(maxAdjacentPathLastNode).size() == 0) {
//                breedingEdgeIdx = rand.nextInt(path.size() - 1);
//                maxAdjacentPathLastNode = path.get(breedingEdgeIdx).getTerminal(false);
//            }
//            mxICell newBornEdge = getRandomOutgoingEdgeFromNode(maxAdjacentPathLastNode);
//            path.add(breedingEdgeIdx + 1, newBornEdge);
//            calculateFitness();
//        }
//        catch (Exception e) {
//            LOG.error(e.getMessage());
//            throw new Exception("Unable to breed chromosome " + this);
//        }
//        if (path.size() != pathLength + 1) {
//            throw new Exception("Breeding changed too much the length of chromosome " + this);
//        }
    }

    /**
     * Remove from the chromosome's path all genes that are after the adjacent part of the path.
     *
     * @throws Exception dissolve operation failed.
     */
    public void dissolve() throws Exception {
        int originalPathLength = path.size();
        try {
            Iterator<mxICell> pathIterator = path.iterator();
            mxICell previous = pathIterator.next();
            mxICell edge;
            int dissolvingEdgeIdx = 0;
            while(pathIterator.hasNext()) {
                edge = pathIterator.next();
                if (previous.getTerminal(false).equals(edge.getTerminal(true))) { // edges are connected
                    // the edges are different and connected
                    dissolvingEdgeIdx++;
                } else { // disconnected by end node in the middle of the path
                    break;
                }
                previous = edge;
            }
            for (int i = dissolvingEdgeIdx + 1; i < path.size(); i++) {
                path.remove(path.get(i));
            }
            this.resetFitness();
        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new Exception("Unable to dissolve a gene in the chromosome " + this);
        }
    }

    /**
     * Turn the fitness values of the chromosome to theirs default values (-1)
     */
    public void resetFitness() {
        this.qualityFitness = -1;
        this.adjacencyFitness = -1;
        this.lczEdgesFitness = -1;
        this.lengthFitness = -1;
        this.fitness = -1;
    }

    public double getAdjacencyFitness() {
        if (adjacencyFitness == -1) {
            calculateAdjacencyFitness();
        }
        return adjacencyFitness;
    }

    /**
     * Adjacency fitness equals: (sum of adjacent edges) / (total number of edges in a chromosome)
     */
    private void calculateAdjacencyFitness() {
        double maxAdjacentEdgesCount = 1;
        double adjacentEdgesCount = 1;
        Iterator<mxICell> pathIterator = path.iterator();
        mxICell previous = pathIterator.next();
        mxICell edge;
        while (pathIterator.hasNext()) {
            edge = pathIterator.next();
            if (!previous.equals(edge)
                    && previous.getTerminal(false).equals(edge.getTerminal(true))) {
                // the edges are different and connected
                adjacentEdgesCount++;
            } else {
                adjacentEdgesCount = 1;
            }
            if (maxAdjacentEdgesCount < adjacentEdgesCount) {
                maxAdjacentEdgesCount = adjacentEdgesCount;
            }
            previous = edge;
        }
        int pathEndsInEndNodeConstant = model.getOutgoingEdgesAsList(path.get(path.size() - 1).getTerminal(false)).isEmpty() ? 0 : 1;
        this.adjacencyFitness = maxAdjacentEdgesCount / (path.size() + pathEndsInEndNodeConstant);
    }

    public double getQualityFitness() {
        if (qualityFitness == -1) {
            calculateQualityFitness();
        }
        return qualityFitness;
    }

    /**
     * Quality fitness equals: (sum covered border nodes) / (total number of uncovered border nodes)
     */
    private void calculateQualityFitness() {
        double noOfBorderNodes = 0;
        for (mxICell key : remainingBorderNodesMap.keySet()) {
            noOfBorderNodes++;
            noOfBorderNodes += remainingBorderNodesMap.get(key).size();
        }
//        Set<mxICell> coveringInNodes = new HashSet<>();
//        mxICell target = null;
//        for (mxICell edge : path) {
//            mxICell source = edge.getTerminal(true);
//            target = edge.getTerminal(false);
//            if (remainingBorderNodesMap.containsKey(source)) {
//                coveringInNodes.add(source);
//            }
//        }
//        if (remainingBorderNodesMap.containsKey(target)) {
//            coveringInNodes.add(target);
//        }
//        return coveringInNodes.size() / noOfCombinations;
        if (noOfBorderNodes > 0) {
            double coveringCombinations = borderNodesCover(false);
            this.qualityFitness = coveringCombinations / noOfBorderNodes;
            this.qualityFitness /= Math.sqrt(path.size());
//            if (coveringCombinations > 0) {
//                this.qualityFitness = 1;
//            } else {
//                this.qualityFitness = 0;
//            }
        } else { // There are no combinations left in the remainingBorderNodesMap
            this.qualityFitness = 1;
        }
    }

    public double getLczEdgesFitness() {
        if (lczEdgesFitness == -1) {
            calculateLczEdgesFitness();
        }
        return lczEdgesFitness;
    }

    private void calculateLczEdgesFitness() {
        double lczEdgesCount = 0;
        Iterator<mxICell> pathIterator = path.iterator();
        mxICell edge;
        while (pathIterator.hasNext()) {
            edge = pathIterator.next();
            boolean isLCZEdge = ((AbstractGraphCell) edge.getValue()).getLimitedConnectionProbability() >= threshold;
            if (isLCZEdge) {
                // the edges are different and connected
                lczEdgesCount++;
            }
        }
        this.lczEdgesFitness = lczEdgesCount / path.size();
    }

    public double getLengthFitness() {
        if (lengthFitness == -1) {
            calculateLengthFitness();
        }
        return lengthFitness;
    }

    private void calculateLengthFitness() {
        this.lengthFitness = 1.0 / Math.sqrt(path.size());
    }

    /**
     * Fitness of chromosome t equals:
     * ((adjacency_t^alpha)*(quality_t^beta))
     * /
     * (sum_i_\<0;chromosomesCount\>/0;chromosomesCount>((adjacency_i^alpha)*(quality_i^beta)))
     */
    private void calculateFitness() {
        // TODO test and enhance
        if (adjacencyFitness == -1) {
            calculateAdjacencyFitness();
        }
        if (qualityFitness == -1) {
            calculateQualityFitness();
        }
        if (lczEdgesFitness == -1) {
            calculateLczEdgesFitness();
        }
        if (lengthFitness == -1) {
            calculateLengthFitness();
        }
//        if (adjacencyFitness == 1) {
//            this.fitness = 1;
//        } else {
//            this.fitness = (Math.pow(adjacencyFitness, GAGenerator.ALPHA) * Math.pow(qualityFitness, GAGenerator.BETA))
//                    / (sumAdjacencyQualityFitness);
//            this.fitness = ((adjacencyFitness * (1.0 / 3.0)) + (qualityFitness * (1.0 / 3.0)) + (lczFitness * (1.0 / 3.0))) * 1/path.size();
//        this.fitness = ((adjacencyFitness * (1.0 / 3.0)) + (qualityFitness * (1.0 / 3.0)) + (lczFitness * (1.0 / 3.0)));
//        this.fitness = ((adjacencyFitness * (1.0 / 4.0)) + (qualityFitness * (1.0 / 4.0)) + (lczFitness * (1.0 / 4.0)) + (lengthFitness * (1.0 / 4.0)));
        this.fitness = (adjacencyFitness * (1.0 / 2.0)) + (qualityFitness * (1.0 / 2.0));
//        }
//        LOG.info("Fitness " + fitness + " (adjacency="+ adjacencyFitness + ", quality=" + qualityFitness + ")");
    }

    /**
     * Combine paths of this chromosome and partner's.
     *
     * @param partner       of this for a crossover
     * @param crossingPoint where path of this should become path of partner
     * @return a new chromosome with left part of this and right part of partner
     */
    public Chromosome leftCrossover(Chromosome partner, int crossingPoint) {
        List<mxICell> offspringPath = new ArrayList<>();
        for (int i = 0; i < crossingPoint; i++) {
            offspringPath.add(path.get(i));
        }
        for (int i = crossingPoint; i < partner.path.size(); i++) {
            offspringPath.add(partner.path.get(i));
        }
        return new Chromosome(offspringPath, model, remainingBorderNodesMap, coverage, threshold);
    }

    public double getFitness() {
        if (fitness == -1) {
            calculateFitness();
        }
        return fitness;
    }

    private mxICell getRandomOutgoingEdgeFromNode(mxICell source) throws Exception {
        List<mxICell> outgoingEdges = model.getOutgoingEdgesAsList(source);
        if (outgoingEdges.size() == 0) {
            throw new Exception("Unable to generate outgoing edges from end node " + source.getValue().toString());
        }
        return outgoingEdges.get(rand.nextInt(outgoingEdges.size()));
    }

    public boolean containDuplicateAdjacentEdges() {
        Set<mxICell> tempSet = new HashSet<>();
        for (mxICell gene : path) {
            if (!tempSet.add(gene)) { // gene already present in the set
                return true;
            }
        }
        return false;
    }

    public boolean pathReachesEnd() {
        return model.getOutgoingEdgesAsList(path.get(path.size() - 1).getTerminal(false)).isEmpty();
    }

    public int getLength() {
        return path.size();
    }

    /**
     * Return the number of uncovered LCZ border nodes according to the current coverage
     * and if the {@code coverNodes} parameter is set to true, changes the {@link Chromosome#remainingBorderNodesMap} variable.
     * The algorithm works as follows:
     * iterate the chromosome's path
     * check if it contains some uncovered LCZ IN node
     * if it does
     * iterate the path from the IN node
     * check, if it is connected and inside the LCZ (it is an LCZ edge)
     * check, if it reaches some uncovered LCZ OUT node (when the path reaches some non-LCZ edge, or one of the end nodes)
     *
     * @param coverNodes The remainingBorderNodesMap is changed accordingly, if it covers some LCZ border nodes
     * @return the number of covered LCZ border nodes combinations
     */
    public int borderNodesCover(boolean coverNodes) {
        // TODO test the correctness of the implementation
        Set<mxICell> coveredInNodes = new HashSet<>(); // queried when removing IN nodes from coveredBorderNodes
        Map<mxICell, Set<mxICell>> coveredBorderNodes = new HashMap<>();
        mxICell prevEdge = null;                // store the edge that precedes the iterated edge
        for (int i = 0; i < path.size(); i++) { // iterate the chromosome's path
            mxICell edge = path.get(i);
            mxICell source = edge.getTerminal(true);
            AbstractGraphCell edgeValue = (AbstractGraphCell) edge.getValue();
            if (coverage.isComprehensiveCoverage() && prevEdge != null && ((AbstractGraphCell) prevEdge.getValue()).getLimitedConnectionProbability() >= threshold) {
                // In case of comprehensive coverage, the previous edge must be non-LCZ
                prevEdge = edge;
                continue;
            }
            if (edgeValue.getLimitedConnectionProbability() >= threshold && remainingBorderNodesMap.containsKey(source)) { // uncovered IN node found; it is the source
                Iterator<mxICell> lczPathIterator = path.listIterator(i);
                mxICell prevLczEdge = prevEdge;
                if (lczPathIterator.hasNext()) {
                    mxICell lczEdge = lczPathIterator.next();
                    mxICell potentialOutNode;
                    while (lczPathIterator.hasNext()) { // iterate the path from the found uncovered IN node
                        prevLczEdge = lczEdge;
                        lczEdge = lczPathIterator.next();
                        if (!prevLczEdge.getTerminal(false).equals(lczEdge.getTerminal(true))) { // the path is not connected
                            break;
                        }
                        potentialOutNode = lczEdge.getTerminal(true);
                        AbstractGraphCell lczEdgeValue = (AbstractGraphCell) lczEdge.getValue();
                        if (coverage.isCompactCoverage() || lczEdgeValue.getLimitedConnectionProbability() < threshold) {
                            if (remainingBorderNodesMap.containsKey(source)) {
                                coveredInNodes.add(source);
                                if (remainingBorderNodesMap.get(source).contains(potentialOutNode)) { // the IN nodes has to be covered by itself
                                    coveredBorderNodes.putIfAbsent(source, new HashSet<>());
                                    coveredBorderNodes.get(source).add(potentialOutNode);
//                                    if (coverNodes) {
                                    LOG.debug("Covering a combination: " + source.getValue().toString() + " - " + potentialOutNode.getValue().toString());
                                    // if the lcz edge targets uncovered OUT node
                                    if (remainingBorderNodesMap.containsKey(source)) {
                                        if (coverNodes) {
                                            remainingBorderNodesMap.get(source).remove(potentialOutNode);
                                            if (remainingBorderNodesMap.get(source).isEmpty() && coveredInNodes.contains(source)) {
                                                remainingBorderNodesMap.remove(source);
                                            }
                                        }
                                        if (coverage.isEachInAndOutNodeOnceCoverage()) {
                                            Iterator<mxICell> lczInIterator = remainingBorderNodesMap.keySet().iterator();
                                            while (lczInIterator.hasNext()) {
                                                mxICell inNode = lczInIterator.next();
                                                if (remainingBorderNodesMap.get(inNode).contains(potentialOutNode) || inNode.equals(potentialOutNode)) {
                                                    coveredBorderNodes.putIfAbsent(inNode, new HashSet<>());
                                                    coveredBorderNodes.get(inNode).add(potentialOutNode);
                                                    if (coverNodes) {
                                                        LOG.debug("Removing: " + inNode.getValue().toString() + " - " + potentialOutNode.getValue().toString());
                                                        remainingBorderNodesMap.get(inNode).remove(potentialOutNode);
                                                        if (remainingBorderNodesMap.get(inNode).isEmpty() && coveredInNodes.contains(inNode)) {
                                                            lczInIterator.remove();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if (lczEdgeValue.getLimitedConnectionProbability() < threshold) { // the edge is at the end of the zone, break from the while
                                break;
                            }
                        }
                    }
                    if (prevLczEdge.getTerminal(false).equals(lczEdge.getTerminal(true))) { // the path is connected
                        if (((AbstractGraphCell) lczEdge.getValue()).getLimitedConnectionProbability() >= threshold) { // the END node is LCZ OUT node condition
                            potentialOutNode = lczEdge.getTerminal(false);
                            if (remainingBorderNodesMap.containsKey(source)) {
                                coveredInNodes.add(source);
                                if (remainingBorderNodesMap.get(source).contains(potentialOutNode)) {
                                    // if the lcz edge targets uncovered OUT node
                                    coveredBorderNodes.putIfAbsent(source, new HashSet<>());
                                    coveredBorderNodes.get(source).add(potentialOutNode);
                                    coveredInNodes.add(source);
                                    if (coverNodes) {
                                        LOG.debug("Covering a combination: " + source.getValue().toString() + " - " + potentialOutNode.getValue().toString());
                                        remainingBorderNodesMap.get(source).remove(potentialOutNode);
                                        if (remainingBorderNodesMap.get(source).isEmpty() && coveredInNodes.contains(source)) {
                                            remainingBorderNodesMap.remove(source);
                                        }
                                    }
                                    if (coverage.isEachInAndOutNodeOnceCoverage()) {
                                        Iterator<mxICell> lczInIterator = remainingBorderNodesMap.keySet().iterator();
                                        while (lczInIterator.hasNext()) {
                                            mxICell inNode = lczInIterator.next();
//                                                if (remainingBorderNodesMap.get(inNode).remove(potentialOutNode)) {
//                                                    LOG.debug("Removing: " + inNode.getValue().toString() + " - " + potentialOutNode.getValue().toString());
//                                                    if (remainingBorderNodesMap.get(inNode).isEmpty() && coveredInNodes.contains(inNode)) {
//                                                        lczInIterator.remove();
//                                                    }
//                                                }
                                            if (remainingBorderNodesMap.get(inNode).contains(potentialOutNode)) {
                                                coveredBorderNodes.putIfAbsent(inNode, new HashSet<>());
                                                coveredBorderNodes.get(inNode).add(potentialOutNode);
                                                if (coverNodes) {
                                                    LOG.debug("Removing: " + inNode.getValue().toString() + " - " + potentialOutNode.getValue().toString());
                                                    remainingBorderNodesMap.get(inNode).remove(potentialOutNode);
                                                    if (remainingBorderNodesMap.get(inNode).isEmpty() && coveredInNodes.contains(inNode)) {
                                                        lczInIterator.remove();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            prevEdge = edge;
        }
        int noOfCoveredInNodes = 0;
        for (mxICell coveredIn : coveredInNodes) {
            if (remainingBorderNodesMap.containsKey(coveredIn)) {
                if (remainingBorderNodesMap.get(coveredIn).isEmpty()) { // ensure that all covered LCZ IN nodes are removed from the remainingBorderNodesMap collection
                    noOfCoveredInNodes++;
                    if (coverNodes) {
                        remainingBorderNodesMap.remove(coveredIn);
                    }
                }
            }
        }

        return coveredBorderNodes.keySet().stream().mapToInt(key -> coveredBorderNodes.get(key).size()).sum() + noOfCoveredInNodes;
    }

    public List<mxICell> getPath() {
        return path;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("|| ");
        sb.append(String.format(Locale.US, "%.2f", getFitness()));
        sb.append(" | ");
        sb.append(LCZGraphUtils.mxICellListToString(path, "-"));
        sb.append(" | length:");
        sb.append(getLength());
        sb.append(" | adjacency:");
        sb.append(getAdjacencyFitness());
        sb.append(" | quality:");
        sb.append(getQualityFitness());
        sb.append(" | lczEdges:");
        sb.append(getLczEdgesFitness());
        sb.append(" | length:");
        sb.append(getLengthFitness());
        sb.append(" | ");
        return sb.toString();
    }

    @Override
    public int compareTo(Chromosome o) {
        int comparison = Double.compare(this.fitness, o.fitness);
        if (comparison == 0) {
            return Integer.compare(this.hashCode(), o.hashCode());
        } else if (comparison < 0) {
            return 1;
        } else {
            return -1;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
