package situations_generators.iot_algorithms.aga;

import com.mxgraph.model.mxICell;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import situations_generators.SituationsGeneratorInterface;
import situations_generators.iot_algorithms.LCZGraphUtils;
import situations_generators.iot_algorithms.LimitedConnectivityZone;
import situations_generators.iot_algorithms.TCsGenerationHelper;
import situations_generators.paper_algorithms.measuring.Stopwatches;
import structure.GraphModelCore;
import structure.IoTCoverage;
import structure.TestSituations;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class AGAGenerator implements SituationsGeneratorInterface {

    private static final Logger LOG = LogManager.getLogger();

    /**
     * Model is used for using its methods.
     */
    private final GraphModelCore model;

    /**
     * Test coverage criterion
     */
    private final IoTCoverage coverage;

    /**
     * This variable stores the threshold for which  we  create  the  test cases T
     */
    private final float threshold;

    private final AtomicBoolean isCanceled = new AtomicBoolean(false);

    private Generation generation;

    // the values of the constants taken from the chiroma2013correlation

    private final double GENE_MUTATION_PROBABILITY = 0.05;

    private final double CHROMOSOME_MUTATION_PROBABILITY = 0.6;

    private final double CROSSOVER_PROBABILITY = 0.6;

    private final double BREEDING_PROBABILITY_OF_GENERATION = 0.8;

    private final double DISSOLVING_PROBABILITY_OF_GENERATION = 0.6;

    public static final int BREEDING_LIMIT = 100;

    public static final int MAX_GENERATIONS = 500;

    public static final int ALG_REPETITIONS = 1000;

    public static final double ALPHA = 3;

    public static final double BETA = 1;

    private final LCZGraphUtils graphUtils;

    public static final int chromosomesCountMultiplicationConstant = 10;

    public static final int chromosomesMaximalCount = 100;
    public static final int chromosomesMinimalCount = 20;

    private final Set<Deque<mxICell>> testSituationsSet;

    private final Map<mxICell, Set<mxICell>> remainingBorderNodesMap;

    public AGAGenerator(GraphModelCore model, IoTCoverage coverage, float threshold) {
        this.model = model;
        this.coverage = coverage;
        this.threshold = threshold;
        testSituationsSet = new HashSet<>();
        graphUtils = new LCZGraphUtils(model, threshold);
        remainingBorderNodesMap = initializeRemainingBorderNodesMap();
        generation = generateInitialGeneration();
    }

    private Map<mxICell, Set<mxICell>> initializeRemainingBorderNodesMap() {
        Map<LimitedConnectivityZone, Map<mxICell, Set<mxICell>>> reachableBorderNodes = graphUtils.findReachableBorderNodes();
        Map<mxICell, Set<mxICell>> remainingBorderNodesMap = new HashMap<>();
        for (LimitedConnectivityZone lcz : reachableBorderNodes.keySet()) {
            remainingBorderNodesMap.putAll(reachableBorderNodes.get(lcz));
        }
        return remainingBorderNodesMap;
    }

    public Generation generateInitialGeneration() {
        List<mxICell> startEdges = new ArrayList<>(model.getOutgoingEdgesAsList(model.getStart()));
        List<mxICell> endEdges = new ArrayList<>();
        List<mxICell> endNodes = graphUtils.getEndNodes();
        for (mxICell end : endNodes) {
            endEdges.addAll(model.getIncomingEdgesAsList(end));
        }
        int chromosomesCount = startEdges.size() * endEdges.size() * chromosomesCountMultiplicationConstant;
        if (chromosomesCount < chromosomesMinimalCount) {
            chromosomesCount = chromosomesMinimalCount;
        } else if (chromosomesCount > chromosomesMaximalCount) {
            chromosomesCount = chromosomesMaximalCount;
        } else if (chromosomesCount % 2 != 0) {
            chromosomesCount++;
        }
        return new Generation(chromosomesCount, startEdges, endEdges, model, remainingBorderNodesMap, coverage, threshold);
    }

    /**
     * Calls the GA operators to create the offsprings and stores them as the current {@link AGAGenerator#generation}.
     *
     * @throws Exception if some operator failed
     */
    public void generateNextGeneration() throws Exception {
        List<Chromosome> offsprings = generation.selectionAndReproduction(GENE_MUTATION_PROBABILITY, CHROMOSOME_MUTATION_PROBABILITY, CROSSOVER_PROBABILITY);
        generation.elitist(offsprings); // propagate the elitist individual in a generation to the next generation if there is some with lower fitness
//        double breedingProbabilityOfGeneration = 2 / Math.sqrt(generation.getGenerationNumber());
//        double dissolvingProbabilityOfGeneration = (- 2 / Math.sqrt(Math.pow(generation.getGenerationNumber(), 2) + 16)) + 1.0/2;
//        double sumAdjacencyQualityFitness = this.generation.sumAdjacencyQualityFitnessOfChromosomes();
        if (generation.getOriginalChromosomesSize() != offsprings.size()) {
            throw new IllegalStateException("The number of offsprings differs from the original number of chromosomes");
        }
        Iterator<Chromosome> offspringsIterator = offsprings.iterator();
        while(offspringsIterator.hasNext()) {
            Chromosome ch = offspringsIterator.next();
//            ch.getFitness(sumAdjacencyQualityFitness);
            ch.getFitness();
            if (ch.getAdjacencyFitness() == 1) {
                LOG.debug("A complete path found!");
                if (ch.getQualityFitness() == 0) {
                    LOG.debug("Remove chromosome without perspective!");
                    offspringsIterator.remove();
                }
            } else {
                if (Math.random() < BREEDING_PROBABILITY_OF_GENERATION) {
//                LOG.info("Before breeding: " + ch);
                    ch.breeding();
//                LOG.info("After breeding: " + ch);
                }
                if (Math.random() < DISSOLVING_PROBABILITY_OF_GENERATION && ch.getPath().size() > 2) {
                    ch.dissolve();
                }
            }
        }
        generation.initialChromosomesToEqualTheSize(offsprings);
        this.generation = new Generation(this.generation.getGenerationNumber() + 1, offsprings, model, remainingBorderNodesMap, coverage, threshold);
    }

    @Override
    public TestSituations generateTestSituations() {
        int algRepetitionNo = 0;
        int generationNo;
        int iterations = 0;
        Stopwatches.getInstance().start();
        LOG.info("The initial border nodes collection: ");
        for (mxICell key : remainingBorderNodesMap.keySet()) {
            LOG.info("\tFor key " + key.getValue().toString() + ": values " +
                    remainingBorderNodesMap.get(key).stream().map(out -> out.getValue().toString()).collect(Collectors.toList()));
        }
        LOG.debug("The initial generation is:\n " + this.generation);
        for (; algRepetitionNo <= ALG_REPETITIONS && !remainingBorderNodesMap.isEmpty(); algRepetitionNo++) {
            generationNo = 0;
            LOG.info("Starting the generation of TCs. The GA was yet repeated: " + (algRepetitionNo) + " time(s)");
            while (!remainingBorderNodesMap.isEmpty() && generationNo++ < MAX_GENERATIONS) {
                LOG.debug("----------- BEGINING OF ROUND [" + (algRepetitionNo * MAX_GENERATIONS + generationNo) + "] ---------------");
                try {
                    iterations++;
                    if (isCanceled.get()) {
                        LOG.debug("The generation of TCs canceled");
                        break;
                    }
                    if (generationNo == MAX_GENERATIONS) {
                        LOG.info(algRepetitionNo + ": MAX_GENERATIONS reached; check TCs in generation");
                        boolean tcFound = generation.handleTCsInGeneration(testSituationsSet);
                        if (tcFound) {
                            if (!remainingBorderNodesMap.isEmpty()) {
                                LOG.debug("Current size of found TCs is: " + testSituationsSet.size());
                                LOG.debug("The number of remaining border nodes combinations is: "
                                        + remainingBorderNodesMap.keySet().stream().mapToInt(key -> remainingBorderNodesMap.get(key).size()).sum());
                                generation = generateInitialGeneration();
                                generationNo = 0;
                            } else {
                                break;
                            }
                        } // ELSE the algorithm will be restarted, if there are some repetitions left
                    } else {
                        if (generation.getChromosomesSize() != generation.getOriginalChromosomesSize()) {
                            generation.complementWithInitialChromosomes();
                        }
                        generateNextGeneration();
                    }
                    LOG.debug("The next generation is:\n " + this.generation);
                    LOG.debug("----------- END OF ROUND [" + (algRepetitionNo * MAX_GENERATIONS + generationNo) + "] ---------------");
                } catch (Throwable e) {
                    e.printStackTrace();
                    LOG.error(e.getCause());
                    return null;
                }
            }
            LOG.debug("The last generation is:\n " + this.generation);
            LOG.debug("The remaining border nodes: ");
            for (mxICell key : remainingBorderNodesMap.keySet()) {
                LOG.debug("For key " + key.getValue().toString() + ": values " +
                        remainingBorderNodesMap.get(key).stream().map(out -> out.getValue().toString()).collect(Collectors.toList()));
            }
            LOG.debug("The test set is: ");
            for (Deque<mxICell> tc : testSituationsSet) {
                LOG.debug(LCZGraphUtils.mxICellListToString(tc, " - "));
            }
            if (remainingBorderNodesMap.isEmpty() || isCanceled.get()) {
                break;
            }
        }
        LOG.info("Generation of TCs ended. The GA was repeated: " + (algRepetitionNo) + " time(s), "
                + "and the result achieved in " + iterations + " iterations.");
        LOG.info("The test set is: ");
        for (Deque<mxICell> tc : testSituationsSet) {
            LOG.info(LCZGraphUtils.mxICellListToString(tc, " - "));
        } if (!remainingBorderNodesMap.isEmpty()) {
            LOG.info("The generation of TCs was unsuccessful. Remaining border nodes combinations: ");
            for (mxICell key : remainingBorderNodesMap.keySet()) {
                LOG.info("\tFor key " + key.getValue().toString() + ": values " +
                        remainingBorderNodesMap.get(key).stream().map(out -> out.getValue().toString()).collect(Collectors.toList()));
            }
            return null;
        }
        Stopwatches.getInstance().stop();
        return TCsGenerationHelper.reverseTestSituations(new HashSet<>(), new LinkedList<>(testSituationsSet));
    }

    public Generation getGeneration() {
        return generation;
    }

    @Override
    public void setCancelled() {
        isCanceled.set(true);
    }
}
